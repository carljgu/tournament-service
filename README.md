# Tournament Service

This is a service tool for setup and result for a multi-team tournament. The application is originally designed for floorball, but can be used any multi-team tournament. It is created for the course IDATT1002 by Carl Gützkow, Nicolai Brand, Callum Gran, Runar Indahl, Eiliert Hansen and Brage Kvamme. More information about the project can be found in the [wiki](https://gitlab.stud.idi.ntnu.no/carljgu/tournament-service/-/wikis/home).

# Installation Guide
To install the program, go to [the installtion manual](https://gitlab.stud.idi.ntnu.no/carljgu/tournament-service/-/wikis/System/Installation-manual)


# Clone and run the repository
You will need git and Maven to be able to clone and run the program on your own computer.

To clone the repository type:

`$ git clone https://gitlab.stud.idi.ntnu.no/carljgu/tournament-service`

Once cloned, type:

`$ cd tournament-service && mvn javafx:run `

to run the program.
