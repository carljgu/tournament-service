/**
 * File that contains all modules.
 *
 * @author Callum Gran
 * @version 0.2
 */
module edu.ntnu.idatt1002.k1g4 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires java.desktop;
    requires com.jthemedetector;
    requires java.sql;

    opens edu.ntnu.idatt1002.k1g4.client to javafx.fxml, com.jthemedetector;
    opens edu.ntnu.idatt1002.k1g4.client.controllers to javafx.fxml;
    exports edu.ntnu.idatt1002.k1g4.client.controllers;
    exports edu.ntnu.idatt1002.k1g4.client;
    exports edu.ntnu.idatt1002.k1g4.dao;
    exports edu.ntnu.idatt1002.k1g4;
}
