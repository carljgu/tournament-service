package edu.ntnu.idatt1002.k1g4;

import edu.ntnu.idatt1002.k1g4.client.App;

/**
 * The type Main.
 *
 * @author Callum
 * @version 0.1
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        App.main(args);
    }
}
