package edu.ntnu.idatt1002.k1g4;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * The class Match which holds
 * two teams.
 *
 * @author Callum Gran, Brage H. Kvamme, Carl Gützkow, Runar Indahl, Nicolai H. Brand.
 * @version 0.8
 */
public class Match {

    private int matchId;
    private int divisionId;
    private Team teamOne;
    private Team teamTwo;
    private int[] score;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String field;
    private boolean isFinished;
    private boolean isWalkover;
    private boolean isKnockout = false;
    private long duration;

    /**
     * First constructor for match.
     *
     * @param teamOne    Team:                  Competing team 1.
     * @param teamTwo    Team:                  Competing team 2.
     * @param startTime  LocalDateTime:         Time of when the match starts
     * @param duration   LocalDateTime:         The expected duration of the match in minutes
     * @param field      String:                The field that the match is played on.
     * @param isWalkover boolean:               Defines if the match is a walkover.
     * @throws IllegalArgumentException thrown if field number is negative, if the teams are the same, if one of the teams is not competing or if endTime is before starTime.
     * @throws NullPointerException     thrown if either team is null or matchType is null
     */
    public Match(Team teamOne, Team teamTwo, LocalDateTime startTime, long duration,
                 String field, boolean isWalkover) throws IllegalArgumentException, NullPointerException {
        if (field.isBlank()) throw new IllegalArgumentException("Field is invalid");
        if (teamOne == null || teamTwo == null) throw new NullPointerException("Team is not defined");
        if (teamOne.equals(teamTwo)) throw new IllegalArgumentException("Teams cannot be the same");
        if (!teamOne.isCompeting() || !teamTwo.isCompeting()) throw new IllegalArgumentException("teams must be competing");
        if (duration < 0) throw new IllegalArgumentException("Duration can not be negative");
        this.teamOne = teamOne;
        this.teamTwo = teamTwo;
        this.score = new int[]{0,0};
        this.duration = duration;
        this.startTime = startTime;
        this.endTime = startTime.plusMinutes(duration);
        this.field = field;
        this.isFinished = false;
        this.isWalkover = isWalkover;
    }

    /**
     * Instantiates a new Match.
     *
     * @param teamOne    Team:      Competing team 1.
     * @param teamTwo    Team:      Competing team 2.
     * @param startTime  Team:      Time of when the match starts
     * @param duration   Team:      Time of when then match ends
     * @param field      Team:      The field that the match is played on.
     */
    public Match(Team teamOne, Team teamTwo, LocalDateTime startTime, long duration, String field) {
        this(teamOne, teamTwo, startTime, duration, field, false);
        this.score = new int[]{0,0};
        this.isFinished = false;
        this.isKnockout = true;
    }


    /**
     * Instantiates a new Match.
     *
     * @param teamOne    Team:     Competing team 1.
     * @param teamTwo    Team:     Competing team 2.
     */
    public Match(Team teamOne, Team teamTwo) {
        this(teamOne, teamTwo, LocalDateTime.now(), 20, "default", false);
        this.score = new int[]{0,0};
        this.isFinished = false;
    }


    /**
     * Instantiates a new Match.
     * This constructor is for data from the database.
     */
    public Match() {}

    /**
     * Increments score for a team.
     *
     * @param teamToIncreaseScore Team:           team which score will increase
     */
    public void incrementScore(Team teamToIncreaseScore) {
        if (teamToIncreaseScore.equals(this.teamOne)) {
            this.score[0]++;
        } else {
            this.score[1]++;
        }
    }

    /**
     * Update score.
     *
     * @param teamNr   int:            the team nr
     * @param newScore int:            the new score
     */
    public void updateScore(int teamNr, int newScore) {
        if (newScore >= 0) {
            if (teamNr == 1)
                this.score[0] = newScore;
            else if (teamNr == 2)
                this.score[1] = newScore;
        }
    }

    /**
     * Get score of a team
     *
     * @param team Team:          the team
     * @return score int:          score of a team
     */
    public int getScore(Team team) {
        if (team.equals(this.teamOne)) {
            return this.score[0];
        }
        return this.score[1];
    }

    /**
     * Gets the score of the match.
     *
     * @return score int[]           the scores of both teams.
     */
    public int[] getScores() {
        return score;
    }

    /**
     * checks if match is finished
     *
     * @return isFinished boolean:            true if match is finished and false if not
     */
    public boolean isFinished() {
       return isFinished;
    }

    /**
     * Sets the match to finished.
     * finishes match with end time now
     * Uses the other finished method to
     * set end time to now.
     */
    public void setFinished() {
        setFinished(LocalDateTime.now());
        if (isKnockout) {
            setWinner();
        }
    }

    /**
     * Set finished.
     * finishes match with specified end time
     *
     * @param endTime LocalDateTime:            the match end time
     */
    public void setFinished(LocalDateTime endTime){
        isFinished = true;
        this.endTime = endTime;
        if (isKnockout)
            setWinner();
    }

    /**
     * Sets the losing team to not competing.
     * Only called on matches that are of type knockout
     */
    public void setWinner() {
        if (score[0] > score[1]) teamTwo.setCompeting(false);
        else teamOne.setCompeting(false);
    }

    /**
     * Sets walkover to true or false.
     *
     * @param walkover boolean:            true if the match is a walk over match
     */
    public void setWalkover(boolean walkover) {
        isWalkover = walkover;
    }

    /**
     * Get team one.
     *
     * @return teamOne Team:           one of the two teams that are playing.
     */
    public Team getTeamOne() {
        return teamOne;
    }

    /**
     * Get team two.
     *
     * @return teamTwo Team:           one of the two teams that are playing.
     */
    public Team getTeamTwo() {
        return teamTwo;
    }

    /**
     * Gets the duration of the match
     *
     * @return duration long:           the duration of the match
     */
    public long getDuration() {
        return duration;
    }

    /**
     * Sets a new duration for the match
     * Should be possible to edit duration
     * after a match is finished in case
     * someone where to forget to finish
     * a match. Also updates the end time
     * accordingly.
     *
     * @param duration long:           the duration of the match
     */
    public void setDuration(long duration) {
        this.duration = duration;
        this.endTime = startTime.plusMinutes(duration);
    }

    /**
     * Gets the start time of the match.
     *
     * @return startTime LocalDateTime:          the start time of the match.
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * Set start time to a new one
     * Updates the duration as well
     *
     * @param startTime LocalDateTime:          the new star time of the match
     */
    public void setStartTime(LocalDateTime startTime){
        this.startTime =startTime;
        this.endTime = startTime.plusMinutes(duration);
    }

    /**
     * Gets the end time of the match.
     *
     * @return endTime LocalDateTime:          the end time of the match.
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * Get the amount of time left of the match.
     * Format is a long in minutes
     *
     * @return long :     minutes left of the match
     */
    public long getTimeLeft() {
        return ChronoUnit.MINUTES.between(LocalDateTime.now(), endTime);
    }

    /**
     * Gets the field of the match.
     *
     * @return field String:              the field number.
     */
    public String getField() {
        return field;
    }

    /**
     * Sets a new field to the match
     *
     * @param field String:             the field for the match
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * Checks if field is occupied by a match
     *
     * @param match Match:          the match to check
     * @return boolean :        true if field and time overlap, false if not
     */
    public boolean fieldOccupied(Match match){
        if (this.getField().equals(match.getField())){
            if (match.getStartTime().isAfter(this.getStartTime().minusMinutes(1L)) && match.getStartTime().isBefore(this.getEndTime())){
                return true;
            }
            if (match.getEndTime().isAfter(this.getStartTime().minusMinutes(1L)) && match.getEndTime().isBefore(this.getEndTime())){
                return true;
            }
        }
        return false;
    }


    /**
     * checks if team is occupied by match
     *
     * @param match Match:          the match to check
     * @return boolean :        true if teams and time overlap, false if not
     */
    public boolean teamsOccupied(Match match){
       boolean teamOneEqualsTeamOne = this.getTeamOne().equals(match.getTeamOne());
       boolean teamOneEqualsTeamTwo = this.getTeamOne().equals(match.getTeamTwo());
        boolean teamTwoEqualsTeamOne = this.getTeamTwo().equals(match.getTeamOne());
        boolean teamTwoEqualsTeamTwo = this.getTeamTwo().equals(match.getTeamTwo());
        if (teamOneEqualsTeamOne || teamOneEqualsTeamTwo || teamTwoEqualsTeamOne || teamTwoEqualsTeamTwo ){
            if (match.getStartTime().isAfter(this.getStartTime().minusMinutes(1L)) && match.getStartTime().isBefore(this.getEndTime())){
                return true;
            }
            if (match.getEndTime().isAfter(this.getStartTime().minusMinutes(1L)) && match.getEndTime().isBefore(this.getEndTime())){
                return true;
            }
        }
        return false;
    }


    /**
     * Is the match walkover.
     *
     * @return isWalkover boolean:               the boolean if the match is a walkover or not.
     */
    public boolean isWalkover() {
        return isWalkover;
    }

    /**
     * Gets match id.
     *
     * @return int : The match id
     */
    public int getMatchId() {
        return matchId;
    }

    /**
     * Sets match id.
     *
     * @param matchId int:   The match id
     */
    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    /**
     * Sets team one.
     *
     * @param teamOne Team: Team one
     */
    public void setTeamOne(Team teamOne) {
        this.teamOne = teamOne;
    }

    /**
     * Sets team two.
     *
     * @param teamTwo Team: Team two
     */
    public void setTeamTwo(Team teamTwo) {
        this.teamTwo = teamTwo;
    }

    /**
     * Get score.
     *
     * @return score int[]: The score of both teams.
     */
    public int[] getScore() {
        return score;
    }

    /**
     * Sets score.
     *
     * @param score int[]: the score of both teams
     */
    public void setScore(int[] score) {
        this.score = score;
    }

    /**
     * Sets end time.
     *
     * @param endTime LocalDateTime: The end time
     */
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    /**
     * Sets finished.
     *
     * @param finished boolean: true or false if match is finished
     */
    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    /**
     * Is knockout boolean.
     *
     * @return isKnockout boolean: true or false if is a knockout match
     */
    public boolean isKnockout() {
        return isKnockout;
    }

    /**
     * Gets division id.
     *
     * @return divisionId int: the division id
     */
    public int getDivisionId() {
        return divisionId;
    }

    /**
     * Sets division id.
     *
     * @param divisionId the division id
     */
    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    /**
     * Sets knockout.
     *
     * @param isKnockout the is knockout
     */
    public void setKnockout(boolean isKnockout) {
        this.isKnockout = isKnockout;
    }

    /**
     * Overrides the equals method from Object
     *
     * @param o     Object:              object to be compared
     * @return      boolean:             true if they are the same, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Match)) return false;
        Match match = (Match) o;
        return (
            this.getField().equals(match.getField()) &&
            teamOne.equals(match.teamOne) &&
            teamTwo.equals(match.teamTwo) &&
            Objects.equals(this.getStartTime(), match.getStartTime())
        );
    }

    /**
     * Overrides the toString method in Object
     * Different for finished, ongoing and not started matches
     *
     * @return      String:             string representation of a match
     */
    @Override
    public String toString() {
        long timeLeft = getTimeLeft();
        String time = timeLeft + " min left";
        if (timeLeft < 0) time = -timeLeft + " min overtime";


        if (this.isFinished()) {
            return (
                    "Match:     " + this.getTeamOne().getName() + " vs " + this.getTeamTwo().getName() +
                    "\n     Finished" +
                    "\n     Score:                " + this.score[0] + " - " + this.score[1] +
                    "\n     End time:          " + this.getEndTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma")) +
                    "\n     Field :                " + this.getField()
            );
        }
        if (startTime.isBefore(LocalDateTime.now())) {
            return (
                    "Match:     " + this.getTeamOne().getName() + " vs " + this.getTeamTwo().getName() +
                    "\n     Ongoing:           " + time +
                    "\n     Score:                " + this.score[0] + " - " + this.score[1] +
                    "\n     Start time:         " + this.getStartTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma")) +
                    "\n     EST End time:  " + this.getEndTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma")) +
                    "\n     Field:                 " + this.getField());
        }
        return (
                "Match:     " + this.getTeamOne().getName() + " vs " + this.getTeamTwo().getName() +
                    "\n     Length:              " + duration + "min" +
                    "\n     Start time:          " + this.getStartTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma")) +
                    "\n     Field:                  " + this.getField());
    }
}
