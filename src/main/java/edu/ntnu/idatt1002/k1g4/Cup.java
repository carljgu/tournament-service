package edu.ntnu.idatt1002.k1g4;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * The type Cup.
 * A Cup has multiple divisions corresponding to different age and gender groups.
 *
 * @author Nicolai H. Brand, Runar Indahl, Carl Gützkow, Callum Gran
 * @version 0.5
 */
public class Cup {

    private int cupId;
    private String name;
    private String location;
    private ArrayList<Division> divisions;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    /**
     * Instantiates a new Cup.
     *
     * @param name      String:                         The name of the cup
     * @param location  String:                         The location of the cup.
     * @param divisions HashMap:                        List of divisions for this cup.
     * @param startTime LocalDateTime:                  When the cup starts.
     * @param endTime   LocalDateTime:                  When the cup ends.
     * @throws IllegalArgumentException If end time before start time.
     */
    public Cup(String name, String location, ArrayList<Division> divisions, LocalDateTime startTime, LocalDateTime endTime) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name for a cup cannot be empty.");
        if (endTime.isBefore(startTime)) throw new IllegalArgumentException("End time cannot be before start time");
        this.name = name;
        this.location = location;
        this.divisions = divisions;
        if (endTime.isBefore(startTime)) throw new IllegalArgumentException("End time cannot be before start time");
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     * Simplified constructor for instantiation of a new Cup.
     *
     * @param name      String:                         The name of the cup
     * @param location  String:                         The location of the cup.
     * @param startTime LocalDateTime:                  When the cup starts.
     * @param endTime   LocalDateTime:                  When the cup ends.
     */
    public Cup(String name, String location, LocalDateTime startTime, LocalDateTime endTime) {
        this(name, location, new ArrayList<>(), startTime, endTime);
    }


    /**
     * Instantiates a new Cup.
     * This constructor is for data from the database.
     */
    public Cup() {}

    /**
     * Gets the name of the cup
     *
     * @return name String:      the name of the cup
     */
    public String getName() {
        return name;
    }

    /**
     * Changes the name of the cup
     *
     * @param name String:       the name of the cup
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets location for where the cup is held.
     *
     * @return Location String:       The cup location.
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the location for where the cup is held
     *
     * @param location String:      the location of the cup
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Get-method which returns the start time of the cup.
     *
     * @return startTime LocalDateTime:         The start time.
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * Get-method which returns the end time of the cup.
     *
     * @return endTime LocalDateTime:       The end time.
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * Mutation-method which sets start time for a cup.
     *
     * @param startTime LocalDateTime:      The start time.
     */
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    /**
     * Mutation-method which sets end time for a cup.
     *
     * @param endTime LocalDateTime:        The end time.
     */
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    /**
     * Returns a list of divisions with composition.
     *
     * @return newDivisions ArrayList:      an arraylist of divisions
     */
    public ArrayList<Division> getDivisions() {
        ArrayList<Division> newDivisions = new ArrayList<>();
        divisions.forEach(n -> newDivisions.add(new Division(n.getCompetingTeams(),
                n.getMatches(), n.getDivisionCategory(), n.getCupId())));
        return newDivisions;
    }

    /**
     * Method which adds division to the list of divisions.
     *
     * @param division Division:   Division to be added.
     * @return Boolean :    True if division doesn't exist and is added, false if it doesn't exist.
     */
    public boolean addDivision(Division division) {
        if (!divisions.contains(division)) {
            divisions.add(division);
            return true;
        } else return false;
    }

    /**
     * Method which removes division to the list of divisions.
     *
     * @param division Division:   Division to be removed.
     */
    public void removeDivision(Division division) {
        divisions.remove(division);
    }

    /**
     * Helper method to get the division that
     * is equal to the object that is provided
     * This is to solve the problems with composition
     *
     * @param division Division:        a division to find the equal of
     * @return Division :        a division that is in the list. Might be null
     */
    public Division getEqualDivision(Division division) {
        if (!divisions.contains(division)) return null;
        int indexOfDivision = divisions.indexOf(division);
        return divisions.get(indexOfDivision);
    }

    /**
     * Finds the correct division and adds a
     * team to the division.
     *
     * @param division Division:        a division to find the equal of
     * @param team     Team:            a provided team to add to the division.
     * @return boolean true if the division exists and the team was successfully added.
     */
    public boolean addTeamInDivision(Division division, Team team) {
        Division divisionInList = getEqualDivision(division);
        if (divisionInList == null) return false;
        return divisionInList.addTeam(team);
    }

    /**
     * Finds the correct division and removes a
     * team from the division.
     *
     * @param division Division:        a division to find the equal of
     * @param team     Team:            provided team to remove from the division.
     * @return boolean true if the division exists and the team was successfully removed.
     */
    public boolean removeTeamInDivision(Division division, Team team) {
        Division divisionInList = getEqualDivision(division);
        if (divisionInList == null) return false;
        return divisionInList.removeTeam(team);
    }

    /**
     * Finds the correct division and adds a
     * match to the division.
     *
     * @param division Division:        a division to find the equal of
     * @param match    Match:           a provided match to add to the division.
     * @return boolean true if the division exists and the match was successfully added.
     */
    public boolean addMatchInDivision(Division division, Match match) {
        Division divisionInList = getEqualDivision(division);
        if (divisionInList == null) return false;
        return divisionInList.addMatch(match);
    }

    /**
     * Finds the correct division and adds a
     * match to the division.
     *
     * @param division Division:        a division to find the equal of
     * @param match    Match:           a provided match to remove from the division.
     * @return boolean true if the division exists and the match was successfully removed.
     */
    public boolean removeMatchInDivision(Division division, Match match) {
        Division divisionInList = getEqualDivision(division);
        if (divisionInList == null) return false;
        return divisionInList.removeMatch(match);
    }

    /**
     * Sets cup id.
     *
     * @param cupId     int:    the cup id
     * @throws IllegalArgumentException the illegal argument exception
     */
    public void setCupId(int cupId) throws IllegalArgumentException {
        if (cupId < 1) throw new IllegalArgumentException("Cup Id must come from the database therefore cannot be less then 1.");
        this.cupId = cupId;
    }

    /**
     * Sets divisions.
     *
     * @param divisions ArrayList: The divisions that the cup have.
     */
    public void setDivisions(ArrayList<Division> divisions) {
        this.divisions = divisions;
    }

    /**
     * Gets cup id.
     *
     * @return int:     the cup id
     */
    public int getCupId() {
        return cupId;
    }

    /**
     * Formats a localDateTime object to dd/MM/yyy hh:mm
     *
     * @return String :      representation of start and estimated end time
     */
    public String formatTime() {
        return  getStartTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma")) + " to "
                + getEndTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mma"));
    }

    /**
     * toString-method for Cup class.
     *
     * @return      string:      representation of a cup
     */
    @Override
    public String toString() {
        ArrayList<Team> teams = new ArrayList<>();
        for (Division division : divisions) {
            for (Team team : division.getCompetingTeams().values()) {
                if (!teams.stream().anyMatch(n -> n.getTeamId() == team.getTeamId())) {
                    teams.add(team);
                }
            }
        }
        int amountOfTeams = teams.size();
        int amountOfMatches = divisions.stream().map(n -> n.getMatches().size()).reduce(0, Integer::sum);

        return getName() + " - " + amountOfTeams + " teams  - "
                + amountOfMatches + " matches - duration: " + formatTime();
    }
}