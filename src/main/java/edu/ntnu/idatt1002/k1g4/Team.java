package edu.ntnu.idatt1002.k1g4;

import java.util.Objects;

/**
 * The team represents a group
 * of competitors playing
 * against another team
 * represented by the class Match
 *
 * @author Callum Gran, Runar Indahl, Nicolai H. Brand, Brage H. Kvamme
 * @version 0.5
 */
public class Team {

    private int teamId;
    private String name;
    private boolean isCompeting;

    /**
     * First constructor for the team.
     *
     * @param name        String:             name of the team
     * @param isCompeting boolean:            true if the team is still eligible for matchmaking
     * @throws IllegalArgumentException thrown if team name is empty.
     */
    public Team(String name, boolean isCompeting)
    throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Team needs a name");
        this.name = name;
        this.isCompeting = isCompeting;
    }

    /**
     * Instantiates a new Team.
     * This team has the isCompeting status set to true by default.
     *
     * @param name   String:      The name of the team
     * @throws IllegalArgumentException the illegal argument exception is thrown when the name is blank.
     */
    public Team(String name) throws IllegalArgumentException {
        this(name, true);
    }


    /**
     * Instantiates a new Team.
     * Copy constructor for Team.
     *
     * @param team Team: the team to copy
     */
    public Team(Team team) {
        this(team.getName(), team.isCompeting());
    }

    /**
     * Instantiates a new Team.
     * This constructor is for data from the database.
     */
    public Team() {}

    /**
     * Gets the team name.
     *
     * @return name String:             the name of the team
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the is competing attribute
     *
     * @return isCompeting boolean:            if the team is competing or not
     */
    public boolean isCompeting() {
        return isCompeting;
    }

    /**
     * Sets new status if the team is or is not competing.
     *
     * @param competing boolean:            if the team is competing or not
     */
    public void setCompeting(boolean competing) {
        isCompeting = competing;
    }

    /**
     * Gets team id.
     *
     * @return teamId int: the team id
     */
    public int getTeamId() {
        return teamId;
    }

    /**
     * Sets team id.
     *
     * @param teamId int: the team id
     */
    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    /**
     * Sets name of the team.
     *
     * @param name String: the name to be set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * overrides the toString method in Object
     * Represents a team on a one liner
     *
     * @return out        String:           String representation of a team
     */
    @Override
    public String toString() {
        String out = " Team: " + name + "   |    ";
        if (isCompeting)
            out += "in competition";
        else
            out += "out of competition";

        return out;
    }

    /**
     * Overrides the equals method from Object
     * Teams are the same if the names are the same.
     *
     * @param o     Object:             object to be compared
     * @return      boolean:            true if the objects are the same, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Team team)) return false;
        return (Objects.equals(name, team.name)
        );
    }

}
