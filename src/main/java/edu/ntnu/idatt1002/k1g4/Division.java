package edu.ntnu.idatt1002.k1g4;

import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;

import java.time.LocalDateTime;
import java.util.*;

/**
 * This class keeps track of a division in the tournament.
 *
 * @author Brage H. Kvamme, Nicolai H. Brand, Runar Indahl, Carl Gützkow, Callum Gran
 * @version 0.7
 */
public class Division {
    private int cupId;
    private int divisionId;
    private HashMap<Integer, Team> competingTeams;
    private ArrayList<Match> matches;
    private String divisionCategory;

    /**
     * Instantiates a new Division.
     *
     * @param competingTeams   HashMap:         the competing teams in this division.
     * @param matches          ArrayList:       the matches that have been played in this division.
     * @param divisionCategory String:          the division which is competed in.
     * @param cupId            int:             the cup id of the division.
     * @throws IllegalArgumentException if category is empty
     */
    public Division(HashMap<Integer, Team> competingTeams,
                    ArrayList<Match> matches, String divisionCategory, int cupId) throws IllegalArgumentException {
        if (divisionCategory.isBlank()) throw new IllegalArgumentException("Division category cannot be empty.");
        this.competingTeams = competingTeams;
        this.matches = matches;
        this.divisionCategory = divisionCategory;
        this.cupId = cupId;
    }

    /**
     * Instantiates a new division.
     * Simplified Constructor which allows a division to be established
     * without a list of teams and matches.
     *
     * @param divisionCategory String:      the division which is competed in.
     * @param cupId            int:         the cup id of the division.
     */
    public Division(String divisionCategory, int cupId) {
        this(new HashMap<>(), new ArrayList<>(), divisionCategory, cupId);
    }


    /**
     * Instantiates a new Division.
     * This constructor is for data from the database.
     */
    public Division() {}

    /**
     * Add a team to the list of competing teams.
     *
     * @param team Team:        a competing team.
     * @return boolean true if team was added, false if not.
     */
    public boolean addTeam(Team team) {
        if (!competingTeams.containsKey(team.getName())) {
            competingTeams.put(team.getTeamId(), team);
            team.setCompeting(true);
            return true;
        } else return false;
    }

    /**
     * Remove a team.
     *
     * @param team Team:        a competing team.
     * @return boolean :     true if team was removed, false if not.
     */
    public boolean removeTeam(Team team) {
        if (competingTeams.containsKey(team.getTeamId())) {
            competingTeams.remove(team.getTeamId(), team);
            team.setCompeting(false);
            return true;
        } else return false;
    }

    /**
     * Add a match to the list of matches.
     *
     * @param match Match:        the match to be added to the division
     * @return Boolean :      True if match not exist and then adds the match. False if already exists.
     * @throws IllegalArgumentException the illegal argument exception
     */
    public boolean addMatch(Match match) throws IllegalArgumentException{
        if(matches.stream().filter(m -> m.fieldOccupied(match)).count() > 0) throw new IllegalArgumentException("Field is occupied");
        if(matches.stream().filter(m -> m.teamsOccupied(match)).count() > 0) throw new IllegalArgumentException("Teams occupied");
        if (!matches.contains(match)) {
            //return matches.add(new Match(match.getTeamOne(), match.getTeamTwo(), match.getStartTime(),
                    //ChronoUnit.MINUTES.between(match.getStartTime(), match.getEndTime()), match.getField(), match.isWalkover()));
            return matches.add(match);
        } else return false;
    }

    /**
     * Remove a match from the list of matches.
     *
     * @param match Match:      the match to be removed.
     * @return boolean :    true when match is removed.
     */
    public boolean removeMatch(Match match) {
        if (matches.contains(match)) {
            return matches.remove(match);
        } else return false;
    }

    /**
     * Creates and return a deep copy of competing teams list.
     *
     * @return HashMap :      a list of competing teams.
     */
    public HashMap<Integer, Team> getCompetingTeams() {
        HashMap<Integer, Team> copy = new HashMap<>();

        for (Team team : competingTeams.values()) {
            Team deepCopiedTeam = new Team(team.getName(), team.isCompeting());
            deepCopiedTeam.setTeamId(team.getTeamId());
            copy.put(deepCopiedTeam.getTeamId(), deepCopiedTeam);
        }
        return copy;
    }

    /**
     * Return the list of matches.
     *
     * @return matches ArrayList:        the list of matches in this division.
     */
    public ArrayList<Match> getMatches() {
        return matches;
    }

    /**
     * Returns the division category.
     *
     * @return divisionCategory String:         the division which is competed in.
     */
    public String getDivisionCategory() {
        return divisionCategory;
    }

    /**
     * Gets the projected amount of matches generated.
     * Integer division by two on total teams
     *
     * @return int : amount of matches to be generatd
     */
    public int getProjectedGeneratedMatches() {
        return (int) (competingTeams.entrySet().stream().filter(t -> t.getValue().isCompeting()).count() / 2);
    }

    /**
     * Helper method to get the match that
     * is equal to the object that is provided
     * This is to solve the problems with composition
     *
     * @param match Match:      Match - a division to find the equal of
     * @return match Match:     a match that is in the list. Might be null
     */
    public Match getEqualMatch(Match match) {
        if (!matches.contains(match)) return null;
        int indexOfMatch = matches.indexOf(match);
        return matches.get(indexOfMatch);
    }


    /**
     * Create random matches.
     *
     * @param startTime the start time
     * @param duration  the duration
     * @param fields    the fields
     * @param divisionId the division id
     * @return the list
     */
    public boolean genRandomMatches(LocalDateTime startTime, int duration, String[] fields, int divisionId) {
        Random random = new Random();
        MatchDAO matchDAO = new MatchDAO();
        ArrayList<String> fieldsAL =  new ArrayList<>(Arrays.asList(fields));

        /* The list of teams that will get partake in bracket */
        ArrayList<Team> inBracket = new ArrayList<>();

        competingTeams.entrySet().stream().filter(t -> t.getValue().isCompeting()).forEach(t -> inBracket.add(t.getValue()));

        while (inBracket.size() >= 2) {
            Team one = inBracket.get(random.nextInt(0, inBracket.size()));
            inBracket.remove(one);
            Team two = inBracket.get(random.nextInt(0, inBracket.size()));
            inBracket.remove(two);

            int newFieldIndex = random.nextInt(0, fieldsAL.size());
            String newField = fieldsAL.get(newFieldIndex).trim();
            fieldsAL.remove(newFieldIndex);

            Match newMatch = new Match(one, two, startTime, duration, newField);
            newMatch.setDivisionId(divisionId);
            newMatch.setKnockout(true);
            matchDAO.addMatch(newMatch);
        }

        return true;
    }

    /**
     * Sets division id.
     *
     * @param divisionId int:   the division id
     * @throws IllegalArgumentException the illegal argument exception
     */
    public void setDivisionId(int divisionId) throws IllegalArgumentException {
        if (divisionId < 1) throw new IllegalArgumentException("Division Id must come from the database therefore cannot be less then 1.");
        this.divisionId = divisionId;
    }

    /**
     * Sets competing teams in a division.
     *
     * @param competingTeams ArrayList: The competing teams
     */
    public void setCompetingTeams(HashMap<Integer, Team> competingTeams) {
        this.competingTeams = competingTeams;
    }

    /**
     * Sets matches in a division.
     *
     * @param matches ArrayList:   The matches
     */
    public void setMatches(ArrayList<Match> matches) {
        this.matches = matches;
    }

    /**
     * Sets division category.
     *
     * @param divisionCategory String: The division category
     * @throws IllegalArgumentException the illegal argument exception
     */
    public void setDivisionCategory(String divisionCategory) throws IllegalArgumentException {
        if (divisionCategory.isBlank()) throw new IllegalArgumentException("The division category cannot be blank");
        this.divisionCategory = divisionCategory;
    }

    /**
     * Gets division id.
     *
     * @return divisionId int :     the division id
     */
    public int getDivisionId() {
        return divisionId;
    }

    /**
     * Sets cup id of the division.
     *
     * @param cupId int: the cup id
     */
    public void setCupId(int cupId) {
        this.cupId = cupId;
    }

    /**
     * Gets cup id.
     *
     * @return cupId int : the cup id
     */
    public int getCupId() {
        return cupId;
    }

    /**
     * override equals method
     *
     * @param o Object:         object to be compared
     * @return  boolean:        true if ageClass and sexClass are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Division)) return false;
        Division division = (Division) o;
        return Objects.equals(competingTeams, division.competingTeams)
                && Objects.equals(matches, division.matches) && divisionCategory.equals(division.divisionCategory);
    }

    /**
     * gets hashcode
     * @return 0 int: the hashcode
     */
    @Override
    public int hashCode() {
        return 0;
    }

    /**
     * toString method
     * @return      String:     toString
     */
    @Override
    public String toString() {
        return getDivisionCategory() + "    -    Competing teams: " + getCompetingTeams().size()
                + "    -   Total matches: " + getMatches().size();
    }
}
