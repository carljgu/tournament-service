package edu.ntnu.idatt1002.k1g4.dao;

import java.sql.*;

/**
 * Class used to create Database connection
 * @author Callum Gran
 * @version 0.1
 */
public class Database {
    private static Database database;

    private static boolean isTest = isJUnitTest();


    private static String DB_URL = "jdbc:sqlite:" + ((isTest) ? "test.db" : "tournament.db");

    /**
     * Checks if there is an existing instance of Database. If not, it creates one.
     * @return database Database: An instance of Database, either the existing one, or a newly created one.
     */
    public static Database instance() {
        if (database == null) {
            database = new Database();
            return database;
        } else {
            return database;
        }
    }

    /**
     * Method to get a connection from the Database object.
     * @return Connection: A connection to the Database object.
     * @throws SQLException if failing to get connection
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL);
    }

    /**
     * Check if connection is from tests or production build
     * @return boolean: true if connection from tests
     */
    private static boolean isJUnitTest() {
        StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();
        for (StackTraceElement element : stackTraces) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Closes connections to database, makes sure that resultSets, and statements gets closed properly
     * @param connection Connection: the connection to be closed
     * @param preparedStatement PreparedStatement: the PreparedStatement to be closed
     * @param resultSet ResultSet: the ResultSet to be closed
     */
    static void close(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
