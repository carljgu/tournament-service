package edu.ntnu.idatt1002.k1g4.dao;

import edu.ntnu.idatt1002.k1g4.Match;
import edu.ntnu.idatt1002.k1g4.Team;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static edu.ntnu.idatt1002.k1g4.dao.Database.close;


/**
 * The Database Access Object for the class Match.
 *
 * @author Callum Gran
 * @version 0.1
 */
public class MatchDAO {

    /**
     * Gets matches in a division.
     *
     * @param divisionId int: the id of the division to get matches from
     * @return matches ArrayList: The matches in a division with all data.
     */
    public ArrayList<Match> getMatchesByDivision(int divisionId) {
        ArrayList<Match> matches = new ArrayList<>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String sql = "SELECT * FROM matches WHERE divisionId = ?;";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, divisionId);
            resultSet = preparedStatement.executeQuery();
            Match match;
            TeamDAO teamDAO;
            while (resultSet.next()) {
                match = new Match();
                teamDAO = new TeamDAO();
                match.setDivisionId(resultSet.getInt("divisionId"));
                match.setTeamOne(new Team(teamDAO.getTeamById(resultSet.getInt("team1Id"), divisionId)));
                match.setTeamTwo(new Team(teamDAO.getTeamById(resultSet.getInt("team2Id"), divisionId)));
                match.getTeamOne().setTeamId(resultSet.getInt("team1Id"));
                match.getTeamTwo().setTeamId(resultSet.getInt("team2Id"));
                match.setScore(new int[] {resultSet.getInt("scoreTeam1"), resultSet.getInt("scoreTeam2")});
                match.setField(resultSet.getString("fieldNumber"));
                match.setFinished(resultSet.getBoolean("finished"));
                match.setWalkover(resultSet.getBoolean("walkover"));
                match.setKnockout(resultSet.getBoolean("knockout"));
                match.setStartTime(LocalDateTime.parse(resultSet.getString("matchStartTime")));
                match.setEndTime(LocalDateTime.parse(resultSet.getString("matchEndTime")));
                match.setDuration(resultSet.getLong("duration"));
                match.setMatchId(resultSet.getInt("matchId"));
                matches.add(match);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return matches;
    }

    /**
     * Gets matches in a cup.
     *
     * @param cupId int: the id of the cup to get matches from
     * @return matches ArrayList: The matches in a cup with all data.
     */
    public ArrayList<Match> getMatchesByCup(int cupId) {
        ArrayList<Match> matches = new ArrayList<>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String sql = "SELECT * FROM matches, divisions WHERE matches.divisionId = divisions.divisionId AND divisions.cupId = ?";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, cupId);
            resultSet = preparedStatement.executeQuery();
            Match match;
            TeamDAO teamDAO;
            while (resultSet.next()) {
                match = new Match();
                teamDAO = new TeamDAO();
                match.setDivisionId(resultSet.getInt("divisionId"));
                match.setTeamOne(new Team(teamDAO.getTeamById(resultSet.getInt("team1Id"), match.getDivisionId())));
                match.setTeamTwo(new Team(teamDAO.getTeamById(resultSet.getInt("team2Id"), match.getDivisionId())));
                match.getTeamOne().setTeamId(resultSet.getInt("team1Id"));
                match.getTeamTwo().setTeamId(resultSet.getInt("team2Id"));
                match.setScore(new int[] {resultSet.getInt("scoreTeam1"), resultSet.getInt("scoreTeam2")});
                match.setField(resultSet.getString("fieldNumber"));
                match.setFinished(resultSet.getBoolean("finished"));
                match.setWalkover(resultSet.getBoolean("walkover"));
                match.setKnockout(resultSet.getBoolean("knockout"));
                match.setStartTime(LocalDateTime.parse(resultSet.getString("matchStartTime")));
                match.setEndTime(LocalDateTime.parse(resultSet.getString("matchEndTime")));
                match.setDuration(resultSet.getLong("duration"));
                match.setMatchId(resultSet.getInt("matchId"));
                matches.add(match);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return matches;
    }


    /**
     * Gets match by id.
     *
     * @param matchId int: the id of the match
     * @return match Match: the match with a given id
     */
    public Match getMatchById(int matchId) {
        Match match = new Match();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String sql = "SELECT * FROM matches WHERE matchId = ?";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, matchId);
            resultSet = preparedStatement.executeQuery();
            TeamDAO teamDAO = new TeamDAO();
            match.setDivisionId(resultSet.getInt("divisionId"));
            match.setTeamOne(new Team(teamDAO.getTeamById(resultSet.getInt("team1Id"), resultSet.getInt("divisionId"))));
            match.setTeamTwo(new Team(teamDAO.getTeamById(resultSet.getInt("team2Id"), resultSet.getInt("divisionId"))));
            match.getTeamOne().setTeamId(resultSet.getInt("team1Id"));
            match.getTeamTwo().setTeamId(resultSet.getInt("team2Id"));
            match.setScore(new int[] {resultSet.getInt("scoreTeam1"), resultSet.getInt("scoreTeam2")});
            match.setField(resultSet.getString("fieldNumber"));
            match.setFinished(resultSet.getBoolean("finished"));
            match.setWalkover(resultSet.getBoolean("walkover"));
            match.setKnockout(resultSet.getBoolean("knockout"));
            match.setStartTime(LocalDateTime.parse(resultSet.getString("matchStartTime")));
            match.setEndTime(LocalDateTime.parse(resultSet.getString("matchEndTime")));
            match.setDuration(resultSet.getLong("duration"));
            match.setMatchId(resultSet.getInt("matchId"));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return match;
    }

    /**
     * Method update match score.
     *
     * @param score   the score
     * @param teamNr  the team nr
     * @param matchId the match id
     */
    public void updateMatchScore(int score, int teamNr, int matchId) {
        String sql;
        if (teamNr == 1) {
            sql = "UPDATE matches SET scoreTeam1 = ? WHERE matchId = ?";
        } else {
            sql = "UPDATE matches SET scoreTeam2 = ? WHERE matchId = ?";
        }
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, score);
            preparedStatement.setInt(2, matchId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }

    /**
     * Method to update match data.
     *
     * @param match Match: the new match data to overwrite the previous data.
     */
    public void updateMatch(Match match) {
        String sql = "UPDATE matches SET divisionId = ?, team1Id = ?, team2Id = ?, scoreTeam1 = ?, scoreTeam2 = ?, duration = ?, matchStartTime = ?, matchEndTime = ?, fieldNumber = ?, finished = ?, walkover = ?, knockout = ? WHERE matchId = ?";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, match.getDivisionId());
            preparedStatement.setInt(2, match.getTeamOne().getTeamId());
            preparedStatement.setInt(3, match.getTeamTwo().getTeamId());
            if (match.getScores() == null) {
                preparedStatement.setInt(4, 0);
                preparedStatement.setInt(5, 0);
            } else {
                preparedStatement.setInt(4, match.getScores()[0]);
                preparedStatement.setInt(5, match.getScores()[1]);
            }
            preparedStatement.setLong(6, match.getDuration());
            preparedStatement.setString(7, match.getStartTime().toString());
            preparedStatement.setString(8, match.getEndTime().toString());
            if (match.isFinished()) {
                TeamDAO teamDAO = new TeamDAO();
                teamDAO.updateCompeting(match.getTeamOne(), match.getDivisionId());
                teamDAO.updateCompeting(match.getTeamTwo(), match.getDivisionId());
            }
            preparedStatement.setString(9, match.getField());
            preparedStatement.setBoolean(10, match.isFinished());
            preparedStatement.setBoolean(11, match.isWalkover());
            preparedStatement.setBoolean(12, match.isKnockout());
            preparedStatement.setInt(13, match.getMatchId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }


    /**
     * Method to add a match to the database.
     *
     * @param match Match: the match to add to the database.
     */
    public void addMatch(Match match) {
        String sql = "INSERT INTO matches(divisionId, team1Id, team2Id, scoreTeam1, scoreTeam2, duration, matchStartTime, matchEndTime, fieldNumber, finished, walkover, knockout) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, match.getDivisionId());
            preparedStatement.setInt(2, match.getTeamOne().getTeamId());
            preparedStatement.setInt(3, match.getTeamTwo().getTeamId());
            if (match.getScores() == null) {
                preparedStatement.setInt(4, 0);
                preparedStatement.setInt(5, 0);
            } else {
                preparedStatement.setInt(4, match.getScores()[0]);
                preparedStatement.setInt(5, match.getScores()[1]);
            }
            preparedStatement.setLong(6, match.getDuration());
            preparedStatement.setString(7, match.getStartTime().toString());
            preparedStatement.setString(8, match.getEndTime().toString());
            preparedStatement.setString(9, match.getField());
            preparedStatement.setBoolean(10, match.isFinished());
            preparedStatement.setBoolean(11, match.isWalkover());
            preparedStatement.setBoolean(12, match.isKnockout());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }

    /**
     * Method to delete a match from the database.
     *
     * @param matchId int: the id of the match to delete.
     */
    public void deleteMatch(int matchId) {
        String sql = "DELETE FROM matches WHERE matchId = ?";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, matchId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }

}