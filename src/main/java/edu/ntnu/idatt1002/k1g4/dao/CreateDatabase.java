package edu.ntnu.idatt1002.k1g4.dao;

import java.sql.*;

/**
 * Class to create a new database with tables.
 * @author Callum Gran
 * @version 0.1
 */
public class CreateDatabase {
    /**
     * Connect to and create a new database
     *
     * @param fileName the database file name
     */
    public static void createNewDatabase(String fileName) {

        try (Connection conn = DriverManager.getConnection("jdbc:sqlite:" + fileName)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            } else {
                System.out.println("Database Connection not found");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Create new database tables.
     *
     * @param fileName the database file name
     */
    public static void createNewTables(String fileName) {
        String url = "jdbc:sqlite:" + fileName;

        String teamsSql = "CREATE TABLE IF NOT EXISTS teams (\n"
                + "	teamId integer PRIMARY KEY,\n"
                + "	teamName text,\n"
                + " competing bit\n"
                + ");";

        String cupsSql = "CREATE TABLE IF NOT EXISTS cups (\n"
                + " cupId integer PRIMARY KEY,\n"
                + " cupName text,\n"
                + " location text,\n"
                + " cupStartTime text,\n"
                + " cupEndTime text\n"
                + ");";

        String divisionsSql = "CREATE TABLE IF NOT EXISTS divisions (\n"
                + " divisionId integer PRIMARY KEY,\n"
                + " cupId integer,\n"
                + " class text,\n"
                + " FOREIGN KEY (cupId) REFERENCES cup(cupId)\n"
                + ");";

        String matchesSql = "CREATE TABLE IF NOT EXISTS matches (\n"
                + " matchId integer PRIMARY KEY,\n"
                + " divisionId integer,\n"
                + " team1Id integer, \n"
                + " team2Id integer, \n"
                + " scoreTeam1 integer,\n"
                + " scoreTeam2 integer,\n"
                + " duration BIGINT,\n"
                + " matchStartTime text,\n"
                + " matchEndTime text,\n"
                + " fieldNumber text,\n"
                + " finished bit,\n"
                + " walkover bit,\n"
                + " knockout bit,\n"
                + " FOREIGN KEY (divisionId) REFERENCES divisions(divisionId)\n"
                + ");";

        String divisionsTeamsSql = "CREATE TABLE IF NOT EXISTS divisionsteamslink(\n"
                + " divisionId integer,\n"
                + " teamId integer,\n"
                + " competing bit, \n"
                + " FOREIGN KEY (divisionId) REFERENCES divisions(divisionId),\n"
                + " FOREIGN KEY (teamId) REFERENCES teams(teamId),\n"
                + " PRIMARY KEY (divisionId, teamId)\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            stmt.execute("PRAGMA foreign_keys = ON");
            stmt.execute(teamsSql);
            stmt.execute(cupsSql);
            stmt.execute(divisionsSql);
            stmt.execute(matchesSql);
            stmt.execute(divisionsTeamsSql);
            System.out.println("Tables created");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}