package edu.ntnu.idatt1002.k1g4.dao;

import edu.ntnu.idatt1002.k1g4.Cup;
import edu.ntnu.idatt1002.k1g4.Division;
import edu.ntnu.idatt1002.k1g4.Match;
import edu.ntnu.idatt1002.k1g4.Team;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import static edu.ntnu.idatt1002.k1g4.dao.Database.close;

/**
 * The Database Access Object for the class Cup.
 *
 * @author Callum Gran
 * @version 0.2
 */
public class CupDAO {

    /**
     * Gets all cups.
     *
     * @return cups ArrayList: All saved cups
     */
    public ArrayList<Cup> getCups() {
        ArrayList<Cup> cups = new ArrayList<>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String sql = "SELECT * FROM cups";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            Cup cup;
            DivisionDAO divisionDAO;
            while (resultSet.next()) {
                cup = new Cup();
                divisionDAO = new DivisionDAO();
                cup.setCupId(resultSet.getInt("cupId"));
                cup.setName(resultSet.getString("cupName"));
                cup.setLocation(resultSet.getString("location"));
                cup.setStartTime(LocalDateTime.parse(resultSet.getString("cupStartTime")));
                cup.setEndTime(LocalDateTime.parse(resultSet.getString("cupEndTime")));
                cup.setDivisions(divisionDAO.getDivisionsByCup(resultSet.getInt("cupId")));
                cups.add(cup);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return cups;
    }


    /**
     * Gets cup based on an already existing cup without an id.
     *
     * @param cup Cup: The cup to fetch from the database.
     * @return cup Cup: the cup from the database
     */
    public Cup getCupByObject(Cup cup) {
        Cup cupToSend = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String sql = "SELECT * FROM cups WHERE cupName = ? AND location = ? AND cupStartTime = ? AND cupEndTime = ?";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, cup.getName());
            preparedStatement.setString(2, cup.getLocation());
            preparedStatement.setString(3, cup.getStartTime().toString());
            preparedStatement.setString(4, cup.getEndTime().toString());
            resultSet = preparedStatement.executeQuery();
            cupToSend = new Cup();
            DivisionDAO divisionDAO;
            divisionDAO = new DivisionDAO();
            cupToSend.setCupId(resultSet.getInt("cupId"));
            cupToSend.setName(resultSet.getString("cupName"));
            cupToSend.setLocation(resultSet.getString("location"));
            cupToSend.setStartTime(LocalDateTime.parse(resultSet.getString("cupStartTime")));
            cupToSend.setEndTime(LocalDateTime.parse(resultSet.getString("cupEndTime")));
            cupToSend.setDivisions(divisionDAO.getDivisionsByCup(resultSet.getInt("cupId")));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return cupToSend;
    }


    /**
     * Method to add a cup to the database.
     *
     * @param cup Cup: The cup to be added to the database
     */
    public void addCup(Cup cup) {
        String sql = "INSERT INTO cups (cupName, location, cupStartTime, cupEndTime) VALUES (?, ?, ?, ?)";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, cup.getName());
            preparedStatement.setString(2, cup.getLocation());
            preparedStatement.setString(3, cup.getStartTime().toString());
            preparedStatement.setString(4, cup.getEndTime().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }

    /**
     * Method to delete a cup from the database.
     *
     * @param cupId int: the id of the cup to be deleted
     */
    public void deleteCup(int cupId) {
        DivisionDAO divisionDAO = new DivisionDAO();
        TeamDAO teamDAO = new TeamDAO();
        MatchDAO matchDAO = new MatchDAO();
        ArrayList<Division> divisions = divisionDAO.getDivisionsByCup(cupId);
        String sql = "DELETE FROM cups WHERE cupId = ?";
        for (Division division : divisions) {
            divisionDAO.deleteDivision(division.getDivisionId());
            HashMap<Integer, Team> teams = teamDAO.getTeamsByDivision(division.getDivisionId());
            teams.forEach((key, value) -> {
                teamDAO.deleteTeam(key, division.getDivisionId());
            });
            ArrayList<Match> matches = matchDAO.getMatchesByDivision(division.getDivisionId());
            for (Match match : matches) {
                matchDAO.deleteMatch(match.getMatchId());
            }
        }
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, cupId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }

}