package edu.ntnu.idatt1002.k1g4.dao;

import edu.ntnu.idatt1002.k1g4.Division;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static edu.ntnu.idatt1002.k1g4.dao.Database.close;

/**
 * The Database Access Object for the class Division.
 * @author Callum Gran
 * @version 0.1
 */
public class DivisionDAO {

    /**
     * Gets all divisions with respective data in a cup.
     *
     * @param cupId int: The id of the cup
     * @return divisions ArrayList: The divisions that belong to a cup
     */
    public ArrayList<Division> getDivisionsByCup(int cupId) {
        ArrayList<Division> divisions = new ArrayList<>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String sql = "SELECT * FROM divisions WHERE cupId = ? ";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, cupId);
            resultSet = preparedStatement.executeQuery();
            Division division;
            MatchDAO matchDAO;
            TeamDAO teamDAO;
            while (resultSet.next()) {
                division = new Division();
                matchDAO = new MatchDAO();
                teamDAO = new TeamDAO();
                division.setCompetingTeams(teamDAO.getTeamsByDivision(resultSet.getInt("divisionId")));
                division.setMatches(matchDAO.getMatchesByDivision(resultSet.getInt("divisionId")));
                division.setDivisionCategory(resultSet.getString("class"));
                division.setCupId(resultSet.getInt("cupId"));
                division.setDivisionId(resultSet.getInt("divisionId"));
                divisions.add(division);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return divisions;
    }

    /**
     * Method to add a division to the database.
     *
     * @param division Division: the division to be added.
     */
    public void addDivision(Division division) {
        String sql = "INSERT INTO divisions (cupId, class) VALUES(?, ?)";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, division.getCupId());
            preparedStatement.setString(2, division.getDivisionCategory());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }

    /**
     * Method to delete a division from the database.
     *
     * @param divisionId int: the id of the division to be deleted
     */
    public void deleteDivision(int divisionId) {
        String sqlDeleteFromTeams = "DELETE FROM divisionsteamslink WHERE divisionsteamslink.divisionId IN (" +
                "SELECT divisionsteamslink.divisionId FROM divisionsteamslink " +
                "INNER JOIN divisions div ON divisionsteamslink.divisionId = div.divisionId WHERE divisionsteamslink.divisionId = ?)";
        String sql = "DELETE FROM divisions WHERE divisionId = ?";
        String sqlTeamDivisionLink = "DELETE FROM divisionsteamslink WHERE divisionId = ?";
        String sqlDeleteDivisionMatches = "DELETE FROM matches WHERE divisionId = ?";
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sqlDeleteFromTeams);
            preparedStatement.setInt(1, divisionId);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, divisionId);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(sqlTeamDivisionLink);
            preparedStatement.setInt(1, divisionId);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(sqlDeleteDivisionMatches);
            preparedStatement.setInt(1, divisionId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }

}