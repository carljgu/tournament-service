package edu.ntnu.idatt1002.k1g4.dao;

import edu.ntnu.idatt1002.k1g4.Team;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static edu.ntnu.idatt1002.k1g4.dao.Database.close;

/**
 * The Database Access Object for the class Team.
 *
 * @author Callum Gran
 * @version 0.1
 */
public class TeamDAO {

    /**
     * Gets all teams.
     *
     * @return teams List: All teams that exist.
     */
    public List<Team> getTeams() {
        List<Team> teams = new ArrayList<>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String sql = "SELECT * FROM teams";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            Team team;
            while (resultSet.next()) {
                team = new Team();
                team.setTeamId(resultSet.getInt("teamId"));
                team.setName(resultSet.getString("teamName"));
                teams.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return teams;
    }

    /**
     * Gets teams in a division.
     *
     * @param divisionId int: the division id
     * @return teams HashMap: The teams in the division.
     */
    public HashMap<Integer, Team> getTeamsByDivision(int divisionId) {
        HashMap<Integer, Team> teams = new HashMap<>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        String sql = "SELECT teams.teamId, teams.teamName, dtl.competing FROM divisions, teams, divisionsteamslink dtl WHERE divisions.divisionId = dtl.divisionId AND divisions.divisionId = ? AND teams.teamId = dtl.teamId";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, divisionId);
            resultSet = preparedStatement.executeQuery();
            Team team;
            while (resultSet.next()) {
                team = new Team();
                team.setTeamId(resultSet.getInt("teamId"));
                team.setName(resultSet.getString("teamName"));
                team.setCompeting(resultSet.getBoolean("competing"));
                teams.put(resultSet.getInt("teamId"), team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return teams;
    }


    /**
     * Gets a team by the name of the team.
     *
     * @param name String: the team name
     * @return team Team: the team with the given name
     */
    public Team getTeamByName(String name) {
        Team team = null;
        String sql = "SELECT * FROM teams WHERE teams.teamName=?";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            try {
                if (resultSet.next()) {
                    team = new Team();
                    team.setTeamId(resultSet.getInt("teamId"));
                    team.setName(resultSet.getString("teamName"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return team;
    }

    /**
     * Gets the team by id.
     *
     * @param teamId int: the id of the team.
     * @param divisionId int: the division id.
     * @return team Team: the team with the corresponding id.
     */
    public Team getTeamById(int teamId, int divisionId) {
        Team team = null;
        String sql = "SELECT teams.teamId, teams.teamName, dtl.competing FROM teams, divisionsteamslink dtl WHERE teams.teamId=? AND dtl.divisionId=? AND dtl.teamId=teams.teamId";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, teamId);
            preparedStatement.setInt(2, divisionId);
            resultSet = preparedStatement.executeQuery();
            team = getTeamFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return team;
    }

    /**
     * Method to get a singular team from a ResultSet.
     * @param resultSet ResultSet: The ResultSet to get a team from.
     * @return team Team: the team from the ResultSet.
     * @throws SQLException
     */
    private Team getTeamFromResultSet(ResultSet resultSet) throws SQLException {
        Team team = null;
        try {
            if (resultSet.next()) {
                team = new Team();
                team.setTeamId(resultSet.getInt("teamId"));
                team.setName(resultSet.getString("teamName"));
                team.setCompeting(resultSet.getBoolean("competing"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return team;
    }


    /**
     * Method to update if a team is competing.
     *
     * @param divisionId int: the division id.
     * @param team Team: the team to update.
     */
    public void updateCompeting(Team team, int divisionId) {
        String sql = "UPDATE divisionsteamslink SET competing = ? WHERE teamId = ? AND divisionId=?";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setBoolean(1, team.isCompeting());
            preparedStatement.setInt(2, team.getTeamId());
            preparedStatement.setInt(3, divisionId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }


    /**
     * Adds a team.
     *
     * @param divisionId int: division id.
     * @param team Team: the team to be added to the database.
     */
    public void addTeam(Team team, int divisionId) throws IllegalArgumentException {
        String sql = "INSERT INTO teams(teamName) VALUES(?)";
        Team teamFromDb = getTeamByName(team.getName());
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        if (teamFromDb == null) {
            try {
                connection = Database.instance().getConnection();
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, team.getName());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {
                close(connection, preparedStatement, resultSet);
            }
        } else {
            String sqlLink = "SELECT * FROM teams, divisionsteamslink WHERE teams.teamId = divisionsteamslink.teamId AND teams.teamName = ? AND divisionsteamslink.divisionId = ?";
            Team newTeamFromDB;
            try {
                connection = Database.instance().getConnection();
                preparedStatement = connection.prepareStatement(sqlLink);
                preparedStatement.setString(1, team.getName());
                preparedStatement.setInt(2, divisionId);
                resultSet = preparedStatement.executeQuery();
                newTeamFromDB = getTeamFromResultSet(resultSet);
                if (newTeamFromDB == null) {
                    linkTeamToDivision(teamFromDb.getTeamId(), divisionId);
                } else {
                    throw new IllegalArgumentException();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                close(connection, preparedStatement, resultSet);
            }
        }
    }

    /**
     * Method to link a team to division.
     *
     * @param teamId     int: the team id
     * @param divisionId int: the division id
     */
    public void linkTeamToDivision(int teamId, int divisionId) {
        String sql = "INSERT INTO divisionsteamslink(divisionId, teamId, competing) VALUES (?, ?, ?)";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, divisionId);
            preparedStatement.setInt(2, teamId);
            preparedStatement.setBoolean(3, true);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }

    /**
     * Method to delete a team from the database.
     *
     * @param teamId int: the id of the team to delete
     * @param divisionId int: the division id
     */
    public void deleteTeam(int teamId, int divisionId) {
        String sqlTeamDivisionLink = "DELETE FROM divisionsteamslink WHERE teamId = ? AND divisionId = ?";
        String sqlTeamMatch = "DELETE FROM matches WHERE team1Id = ? OR team2Id = ? AND divisionId = ?";
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sqlTeamDivisionLink);
            preparedStatement.setInt(1, teamId);
            preparedStatement.setInt(2, divisionId);
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(sqlTeamMatch);
            preparedStatement.setInt(1, teamId);
            preparedStatement.setInt(2, teamId);
            preparedStatement.setInt(3, divisionId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            close(connection, preparedStatement, resultSet);
        }
    }
}