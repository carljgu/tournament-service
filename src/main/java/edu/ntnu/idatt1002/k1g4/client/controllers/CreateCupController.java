package edu.ntnu.idatt1002.k1g4.client.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;


import static edu.ntnu.idatt1002.k1g4.client.Model.createCup;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.changeScene;


/**
 * Cup Controller
 * handles the page of creating a cup
 * Coupled with the create-cup.fxml file
 *
 * @author Carl Gützkow, Nicolai H. Brand.
 * @version 0.4
 */
public class CreateCupController {

    @FXML
    private TextField cupNameField;

    @FXML
    private TextField cupLocationField;

    @FXML
    private DatePicker startDateField;

    @FXML
    private TextField startTimeField;

    @FXML
    private DatePicker endDateField;

    @FXML
    private TextField endTimeField;

    /**
     * Cancel creation of a cup and returns to the home page
     */
    @FXML
    void cancelCreation() {
        changeScene("home");
    }

    /**
     * Create cup. Changes page to the cup overview
     * if creating the cup was successful.
     * Any exception that is thrown is handled by
     * the Model class and shows an alert to the user.
     */
    @FXML
    void onCreateCup() {
        boolean result = createCup(cupNameField, cupLocationField, startDateField,
                startTimeField, endDateField, endTimeField);
        if (result) {
            changeScene("cup-overview");
        }
    }

}