package edu.ntnu.idatt1002.k1g4.client.controllers;

import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import edu.ntnu.idatt1002.k1g4.dao.TeamDAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.changeScene;
import static edu.ntnu.idatt1002.k1g4.client.Model.*;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.*;

/**
 * The match overview controller.
 * Coupled with the match-overview.fxml file
 *
 * @author Runar Indahl, Nicolai H. Brand, Eilert Werner Hansen, Callum Gran
 * @version 0.4
 */
public class MatchOverviewController implements Initializable {

    @FXML
    private Button endMatch;

    @FXML
    private TextField fieldNrField;

    @FXML
    private Label nameTeamOne;

    @FXML
    private Label nameTeamTwo;

    @FXML
    private DatePicker startDateField;

    @FXML
    private TextField startTimeField;

    @FXML
    private TextField teamOneScore;

    @FXML
    private TextField teamTwoScore;

    @FXML
    private TextField timeDurationField;

    @FXML
    private Label timeLeftText;

    /**
     * Changes to the division overview page
     */
    @FXML
    void closeButtonAction() {
        changeScene("division-overview");
    }


    /**
     * Team one score increases.
     */
    @FXML
    public void teamOneScoreIncr() {
        incrementScore(teamOneScore, 1, 1);
    }

    /**
     * Team one score decreases.
     */
    @FXML
    public void teamOneScoreDecr() {
        incrementScore(teamOneScore, -1, 1);
    }

    /**
     * Team two score increases.
     */
    @FXML
    public void teamTwoScoreIncr() {
        incrementScore(teamTwoScore, 1, 2);
    }

    /**
     * Team two score decreases.
     */
    @FXML
    public void teamTwoScoreDecr() {
        incrementScore(teamTwoScore, -1, 2);
    }


    /**
     * Updates the scene with new values
     */
    public void updateMatchValues() {
        updateMatch();
    }


    /**
     * finishes a match and updates the match scene.
     */
    public void endMatchValues() {
        MatchDAO matchDAO = new MatchDAO();
        TeamDAO teamDAO = new TeamDAO();
        getCurrentMatch().setFinished();
        matchDAO.updateMatch(getCurrentMatch());
        getCurrentDivision().setMatches(matchDAO.getMatchesByDivision(getCurrentDivision().getDivisionId()));
        getCurrentDivision().setCompetingTeams(teamDAO.getTeamsByDivision(getCurrentDivision().getDivisionId()));
        changeScene("division-overview");
    }

    /**
     * Increments the score in the GUI
     * @param scoreField    TextField:              field with the score
     * @param incrementBy   int:                    number score should be changed by
     * @param teamNr        int:                    team to get a change in score
     */
    private void incrementScore(TextField scoreField, int incrementBy, int teamNr) {
        int score = Integer.parseInt(scoreField.getText());
        score += incrementBy;
        if (score >= 0) {
            scoreField.setText(String.valueOf(score));
            updateMatchScore(teamNr, score);
        }
    }

    /**
     * Helper method to update all fields for the current match
     */
    private void updateFields(){
        startDateField.setValue(getCurrentMatch().getStartTime().toLocalDate());
        startTimeField.setText(getCurrentMatch().getStartTime().format(DateTimeFormatter.ofPattern("HH:mm")));
        timeDurationField.setText(""+(getCurrentMatch().getDuration()));
        fieldNrField.setText(getCurrentMatch().getField());
    }

    /**
     * Helper method to update time left of the match
     */
    private void upDateTimeLeft(){
        if (getCurrentMatch().isFinished()) {
            timeLeftText.setText("FINISHED");
        }
        else if (getCurrentMatch().getStartTime().isBefore(LocalDateTime.now())) {
            long timeLeft = getCurrentMatch().getTimeLeft();
            String time = timeLeft + " min left";
            if (timeLeft < 0) time = -timeLeft + " min overtime";
            timeLeftText.setText("Ongoing: " + time);
        }
        else{
            timeLeftText.setText("");
        }
    }

    /**
     * Helper method to update all information about the match
     */
    private void updateMatch(){
        LocalDateTime startTime;
        long duration;
        String field;
        MatchDAO matchDAO = null;
        try {
            matchDAO = new MatchDAO();
            startTime = LocalDateTime.of(startDateField.getValue(), LocalTime.parse(startTimeField.getText()));
            duration = Long.parseLong(timeDurationField.getText());
            if (!fieldNrField.getText().isEmpty()) {
                field = fieldNrField.getText();
                getCurrentMatch().setField(field);
            }
            getCurrentMatch().setStartTime(startTime);
            getCurrentMatch().setDuration(duration);
            matchDAO.updateMatch(getCurrentMatch());
        } catch (Exception e){
            giveError(e.getMessage());
        }
    }


    /**
     * Run when the page is loaded in.
     *
     * @param url               URL:                        represents a Uniform Resource Locator
     * @param resourceBundle    ResourceBundle:             contains locale-specific objects
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (getCurrentMatch().isFinished()){
            endMatch.setVisible(false);
        }
        updateFields();
        upDateTimeLeft();

        nameTeamOne.setText(getCurrentMatch().getTeamOne().getName());
        nameTeamTwo.setText(getCurrentMatch().getTeamTwo().getName());

        teamOneScore.setText(String.valueOf(getCurrentMatch().getScores()[0]));
        teamTwoScore.setText(String.valueOf(getCurrentMatch().getScores()[1]));

    }



}
