package edu.ntnu.idatt1002.k1g4.client.controllers;

import static edu.ntnu.idatt1002.k1g4.client.Model.*;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.*;

import edu.ntnu.idatt1002.k1g4.Cup;
import edu.ntnu.idatt1002.k1g4.client.Model;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;

import java.net.URL;
import java.util.ResourceBundle;


/**
 * Home Controller
 * handles the home page
 * and the buttons for go to
 * the cup creation page, help and rules.
 * Coupled with the home.fxml file
 *
 * @author Carl Gützkow, Nicolai H. Brand.
 * @version 0.5
 */
public class HomeController implements Initializable {

    @FXML
    private ListView<Cup> currentCupsList;

    @FXML
    private RowConstraints gridPanelRow1;

    @FXML
    private Label infoText;

    @FXML
    private GridPane gridPane;

    @FXML
    private Button deleteCupField;

    @FXML
    private Button goToCupField;

    @FXML
    private Button newCup;

    /**
     * Overridden method from Initializable
     * that will run each time this controller is loaded.
     * Used to display all cups that are created
     * Button for deleting a match is set to red
     *
     * @param url       represents a Uniform Resource Locator
     * @param resourceBundle        bundle that contains locale-specific objects
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        currentCupsList.getItems().clear();
        gridPanelRow1.setMinHeight(10);
        gridPanelRow1.fillHeightProperty();
        /* make delete button RED */
        deleteCupField.setStyle("-fx-background-color: #ff0000;");
        newCup.setStyle("-fx-background-color: #009804;");

        if (!Model.getCups().isEmpty()) {
            showInfoWhenCups();

            if (currentCupsList.getItems().size() == 1)
                infoText.setText("This is the current cup:");
            else
                infoText.setText("These are the current cups:");

            currentCupsList.getSelectionModel().selectedItemProperty().addListener((a, b, c) -> setCurrentCup(c));

           /* if double click on cup, go straight into it */
           currentCupsList.setOnMouseClicked(click -> {
               if (click.getButton() == MouseButton.PRIMARY && click.getClickCount() == 2) {
                   Cup selectedCup = currentCupsList.getSelectionModel().getSelectedItem();
                   setCurrentCup(selectedCup);
                   changeScene("cup-overview");
               }
           });

            updateListOfCups();

        } else {
            infoText.setText("No previous cups. Click \"New cup\" to start.");
            //hideInfoWhenNoCups();
        }
    }

    /**
     * Helper method to update list of cups.
     * Puts all cups from Model into a list
     * of current cups
     */
    public void updateListOfCups() {
        Model.getCups().forEach(cup -> currentCupsList.getItems().add(cup));
    }

    /**
     * Helper method to show info when cups.
     */
    public void showInfoWhenCups() {
        //gridPane.setVgap(50);
        currentCupsList.setScaleX(1);
        currentCupsList.setScaleY(1);

        deleteCupField.setVisible(true);
        goToCupField.setVisible(true);

        /* update font size */
        currentCupsList.setCellFactory(cell -> new ListCell<>() {
            @Override
            protected void updateItem(Cup item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    setText(String.valueOf(item));
                    setFont(Font.font(22));
                }
            }
        });

        gridPanelRow1.fillHeightProperty();
    }


    /**
     * Helper method to hide info when no cups.
     */
    public void hideInfoWhenNoCups() {
        gridPane.setVgap(0);
        /* Hide current listview and buttons since there are no cups */
        currentCupsList.setScaleX(0);
        currentCupsList.setScaleY(0);
        deleteCupField.setVisible(false);
        goToCupField.setVisible(false);
    }

    /**
     * Changes page to the create cup page
     */
    @FXML
    void goToCreateCup() {
        changeScene("create-cup");
    }

    /**
     * Delete the selected cup if
     * a cup is selected and if
     * user confirms
     */
    @FXML
    void deleteCup() {
        if (getCurrentCup() == null) {
            giveError("There is no cup selected. Click on a cup to select it.");
        }  else {
            boolean res = getConfirmation("Are you sure you want to delete the cup " + getCurrentCup().getName() + " ?");

            if (res) {
                deleteSelectedCup();
                /* refresh scene as the list view needs to be updated */
                changeScene("home");
            }
        }
    }

    /**
     * Changes page to the cup overview
     * for the selected cup.
     * Gives error if no cup is selected.
     */
    @FXML
    void goToCup() {
        if (getCurrentCup() != null)
            changeScene("cup-overview");
        else
            giveError("There is no cup selected. Click on a cup to select it.");
    }

    /**
     * Takes the user to the documentation about how to use the product
     * if it finds a browser to open the link in.
     * Gives error pop up if link can't be opened.
     */
    @FXML
    void getHelp() {
        try {
            openBrowser("https://gitlab.stud.idi.ntnu.no/carljgu/tournament-service/-/wikis/System/User-manual");
        } catch (Exception e) {
            giveError("Could not open link.");
        }


    }

    /**
     * Takes the user to a website with rules about floorball
     * if it finds a browser to open the link in.
     * Gives error pop up if link can't be opened.
     */
    @FXML
    void getRules(){
        try {
            openBrowser("https://floorball.sport/rules-and-regulations/rules-of-the-game/");
        } catch (Exception e) {
            giveError("Could not open link.");
        }
    }

    /**
     * Goes to the cup overview
     */
    @FXML
    void getCup() {
        changeScene("cup-overview");
    }
}