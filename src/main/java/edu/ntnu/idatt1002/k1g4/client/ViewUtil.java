package edu.ntnu.idatt1002.k1g4.client;

import edu.ntnu.idatt1002.k1g4.client.controllers.HomeController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

import static edu.ntnu.idatt1002.k1g4.client.App.getDefaultHeight;
import static edu.ntnu.idatt1002.k1g4.client.App.getDefaultWidth;
import static edu.ntnu.idatt1002.k1g4.client.Model.*;

/**
 * The type View util.
 * This will NOT be a view as in the MVC (model, view, controller) structure.
 * We take ideas and principle from the MVC structure, but adapt it to suit our needs.
 * See the project wiki on gitlab https://gitlab.stud.idi.ntnu.no/carljgu/tournament-service/-/wikis/home
 * for how this class relates to the Controllers and Model.
 *
 * @author Carl J. Gützkow, Nicolai H. Brand.
 * @version 0.4
 */
public class ViewUtil {

    private static Stage stage;
    private static Scene scene;
    private static Alert alert;
    private static String css = "base.css";
    private static String currentScene = "home";

    /**
     * Change scene.
     * Static method that can be called by the respective controllers.
     * Uses the stage in App to change the scene
     *
     * @param fileName String       the file name
     */
    public static void changeScene(String fileName) {
        FXMLLoader fxmlLoader = new FXMLLoader(HomeController.class.getClassLoader().getResource(fileName + ".fxml"));
        currentScene = fileName;
        try {
            scene = new Scene(fxmlLoader.load(), getDefaultWidth(), getDefaultHeight());
            scene.getStylesheets().add(css);

            if (fileName.equals("cup-overview")) {
                setNoCurrentDivision();
                setNoCurrentTeam();
            }

            if (fileName.equals("home"))
                setNoCurrentCup();

            if (fileName.equals("division-overview")) {
                setNoCurrentTeam();
                setNoCurrentMatch();
            }

            App.getStage().setScene(scene);

        } catch (IOException | IllegalStateException e) {
            giveError("Something went severely wrong when trying to load the next page");
        }
    }

    /**
     * Sets css.
     *
     * @param css the css file
     */
    public static void setCss(String css) {
        ViewUtil.css = css;
    }

    /**
     * Gets current scene.
     *
     * @return the current scene
     */
    public static String getCurrentScene() {
        return currentScene;
    }

    /**
     * Give information.
     * Static method that can be called by the respective controllers.
     *
     * @param message String:       the message
     */
    public static void giveInformation(String message) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }

    /**
     * Give error.
     * Static method that can be called by the respective controllers.
     *
     * @param message String:       the message
     */
    public static void giveError(String message) {
        alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Gets confirmation.
     * Static method that can be called by the respective controllers.
     *
     * @param message String:       the message
     * @return Boolean the confirmation
     */
    public static boolean getConfirmation(String message) {
        alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(null);
        alert.setContentText(message);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }
}
