package edu.ntnu.idatt1002.k1g4.client.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;

import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.*;
import static edu.ntnu.idatt1002.k1g4.client.Model.*;

/**
 * Controller for the create division page.
 * Creates a division
 * with the Model class.
 * Coupled with the create-division.fxml file
 *
 * @author Carl Gützkow, Runar Indahl
 * @version 0.3
 */
public class CreateDivisionController {

    @FXML
    private TextField textFieldDivisionCategory;

    /**
     * Changes back to the cup overview scene
     * without changing anything else.
     */
    @FXML
    void cancelCreation() {
        changeScene("cup-overview");
    }

    /**
     * Calls the model class to create a division
     * from the combo box and spinner.
     * If that succeeds, it will change scene.
     */
    @FXML
    void onCreateDivision() {
        boolean divisionCreated = createDivision(textFieldDivisionCategory);
        if (divisionCreated) changeScene("cup-overview");
    }
}
