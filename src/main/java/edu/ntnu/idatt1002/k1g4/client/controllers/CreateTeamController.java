package edu.ntnu.idatt1002.k1g4.client.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.changeScene;
import static edu.ntnu.idatt1002.k1g4.client.Model.createTeam;

/**
 * Controller for creating a team.
 * Coupled with the create-team.fxml file.
 * Used to either create a team
 * or go back to the division overview
 *
 * @author Nicolai H. Brand, Carl Gützkow
 * @version 0.3
 */
public class CreateTeamController {

    @FXML
    private TextField teamNameField = new TextField();

    /**
     * Cancel creation and goes back to the division overview
     */
    @FXML
    void cancelCreation() {
        changeScene("division-overview");
    }

    /**
     * Creates a team and goes to the division overview if
     * it succeeds
     */
    @FXML
    void onCreateTeam() {
        boolean isTeamCreated = createTeam(teamNameField);
        if (isTeamCreated)
            changeScene("division-overview");
    }

}