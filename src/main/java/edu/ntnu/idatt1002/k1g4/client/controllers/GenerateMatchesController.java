package edu.ntnu.idatt1002.k1g4.client.controllers;

import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

import static edu.ntnu.idatt1002.k1g4.client.Model.generateMatches;
import static edu.ntnu.idatt1002.k1g4.client.Model.getCurrentDivision;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.changeScene;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.giveInformation;

/**
 * The type Generate match controller.
 * @author Nicolai H. Brand.
 * @version 0.1
 */
public class GenerateMatchesController implements Initializable {

    @FXML
    private Spinner<Integer> durationField;

    @FXML
    private TextField fieldField;

    @FXML
    private DatePicker startDateField;

    @FXML
    private TextField startTimeField;

    @FXML
    private Label projectionLabel;

    @FXML
    void showHelp() {
        String helpMessage = "How matches are generated.\n\n" +
                "First, pair up teams that are still in the division. This goes on for as long as their are more than\n" +
                "one team without a new match. After this has been done, if there is still one team left without\n" +
                "a match, this team does not get a match and is through to the next round automatically. This is a\n" +
                "consequence of having an odd number of teams in a division.\n" +
                "Of course, the tournament organisers are free to manually create more matches if need be.\n";
        giveInformation(helpMessage);
    }

    /**
     * Cancel creation.
     *
     */
    @FXML
    void cancelCreation() {
        changeScene("division-overview");
    }

    /**
     * On create match.
     *
     */
    @FXML
    void onGenerateMatch() {
        MatchDAO matchDAO = new MatchDAO();
        boolean rc = generateMatches(startDateField, startTimeField, durationField, fieldField);
        getCurrentDivision().setMatches(matchDAO.getMatchesByDivision(getCurrentDivision().getDivisionId()));
        if (rc)
            changeScene("division-overview");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        int projectedNMatches = getCurrentDivision().getProjectedGeneratedMatches();
        String suffix = projectedNMatches == 1 ? " match." : " matches.";
        projectionLabel.setText("Projected to generate " + projectedNMatches + suffix);

        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100);
        valueFactory.setValue(0);
        durationField.setValueFactory(valueFactory);
        durationField.increment(24);
    }

}