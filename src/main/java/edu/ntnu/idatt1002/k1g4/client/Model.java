package edu.ntnu.idatt1002.k1g4.client;

import edu.ntnu.idatt1002.k1g4.*;

import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.giveError;

import edu.ntnu.idatt1002.k1g4.dao.CupDAO;
import edu.ntnu.idatt1002.k1g4.dao.DivisionDAO;
import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import edu.ntnu.idatt1002.k1g4.dao.TeamDAO;
import javafx.scene.control.*;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;


/**
 * The type Model.
 * This will NOT be a model as in the MVC (model, view, controller) structure.
 * We take ideas and principle from the MVC structure, but adapt it to suit our needs.
 * See the project wiki on gitlab https://gitlab.stud.idi.ntnu.no/carljgu/tournament-service/-/wikis/home
 * for how this class relates to the Controllers and ViewUtil.
 *
 * @author Carl Gützkow, Nicolai H. Brand, Runar Indahl, Callum Gran
 * @version 0.9
 */
public class Model {

    private static ArrayList<Cup> cups = new ArrayList<>();
    private static Cup currentCup;
    private static Division currentDivision;
    private static Match currentMatch;
    private static Team currentTeam;

    /**
     * Returns the static list of cups
     *
     * @return cups an arraylist of cups
     */
    public static ArrayList<Cup> getCups() {
        CupDAO cupDAO = new CupDAO();
        cups = cupDAO.getCups();
        return cups;
    }

    /**
     * Returns the current cup set.
     *
     * @return currentCup Cup:       The last cup selected
     */
    public static Cup getCurrentCup() {
        return currentCup;
    }

    /**
     * Sets the current cup to a new value
     *
     * @param newCup Cup:       a cup that is to be selected
     */
    public static void setCurrentCup(Cup newCup) {
        currentCup = newCup;
    }

    /**
     * Sets no current cup.
     */
    public static void setNoCurrentCup() {
        setCurrentCup(null);
    }


    /**
     * Returns the division that is currently selected
     *
     * @return currentDivision Division:         the currently selected division
     */
    public static Division getCurrentDivision() {
        return currentDivision;
    }

    /**
     * Selects a new division as the current one
     *
     * @param newDivision Division:         the new division to be selected
     */
    public static void setCurrentDivision(Division newDivision) {
        currentDivision = newDivision;
    }

    /**
     * Delete selected division.
     */
    public static void deleteSelectedDivision() {
        /* this method will only be called IF currentDivision is not null */
        DivisionDAO divisionDAO = new DivisionDAO();
        divisionDAO.deleteDivision(currentDivision.getDivisionId());
        currentCup.setDivisions(divisionDAO.getDivisionsByCup(currentDivision.getCupId()));
    }

    /**
     * removes the selected cup from the cups arraylist
     */
    public static void deleteSelectedCup() {
        CupDAO cupDAO = new CupDAO();
        cupDAO.deleteCup(getCurrentCup().getCupId());
        cups.remove(getCurrentCup());
    }

    /**
     * Sets no current division.
     */
    public static void setNoCurrentDivision() {
        setCurrentDivision(null);
    }

    /**
     * Returns the match that is currently selected
     *
     * @return currentMatch Match:       the currently selected match
     */
    public static Match getCurrentMatch() {
        return currentMatch;
    }

    /**
     * Selects a new match as the current one
     *
     * @param newMatch Match:       the new match to be selected
     */
    public static void setCurrentMatch(Match newMatch) {
        currentMatch = newMatch;
    }


    /**
     * Delete selected match.
     */
    public static void deleteSelectedMatch() {
        /* this method will only be called IF currentMatch is not null */
        MatchDAO matchDAO = new MatchDAO();
        Match actualMatchToDelete = getCurrentCup().getEqualDivision(getCurrentDivision()).getEqualMatch(getCurrentMatch());
        matchDAO.deleteMatch(actualMatchToDelete.getMatchId());
        getCurrentCup().getEqualDivision(getCurrentDivision()).removeMatch(actualMatchToDelete);
    }

    /**
     * Sets no current match.
     */
    public static void setNoCurrentMatch() {
        setCurrentMatch(null);
    }

    /**
     * Sets current team to null.
     */
    public static void setNoCurrentTeam() {
        setCurrentTeam(null);
    }

    /**
     * returns the selected team
     *
     * @return currentTeam Team:        the current selected team
     */
    public static Team getCurrentTeam() {
        return currentTeam;
    }

    /**
     * sets the current team
     *
     * @param newCurrentTeam Team:      the team to be selected
     */
    public static void setCurrentTeam(Team newCurrentTeam) {
        currentTeam = newCurrentTeam;
    }

    /**
     * Create cup.
     * Gets input information from the CupController.
     * Tries to parse it's input and create a Cup object.
     * Returns true if a new Cup was created and false if not.
     *
     * @param cupNameField     TextField:       the cup name field
     * @param cupLocationField TextField:       the cup location field
     * @param startDateField   DatePicker:      the start date field
     * @param startTimeField   TextField:       the start time field
     * @param endDateField     DatePicker:      the end date field
     * @param endTimeField     TextField:       the end time field
     * @return result boolean          true if the cup was successfully created
     */
    public static boolean createCup(TextField cupNameField, TextField cupLocationField, DatePicker startDateField, TextField startTimeField,
                                    DatePicker endDateField, TextField endTimeField) {
        String cupName;
        String cupLocation;
        LocalDateTime startDateTime;
        LocalDateTime endDateTime;
        Cup cup;
        Cup cupFromDB;
        boolean result = false;

        try {
            CupDAO cupDAO = new CupDAO();
            cupName = cupNameField.getText();
            cupLocation = cupLocationField.getText();
            startDateTime = LocalDateTime.of(startDateField.getValue(), LocalTime.parse(startTimeField.getText()));
            endDateTime = LocalDateTime.of(endDateField.getValue(), LocalTime.parse(endTimeField.getText()));

            cup = new Cup(cupName, cupLocation, startDateTime, endDateTime);
            cupDAO.addCup(cup);
            cups = Model.getCups();
            cupFromDB = cupDAO.getCupByObject(cup);
            setCurrentCup(cupFromDB);
            result = true;
        } catch (IllegalArgumentException e) {
            giveError(e.getMessage());
        } catch (DateTimeParseException e) {
            giveError("Text could not be converted to a valid time.");
        } catch (NullPointerException e) {
            giveError("Some field was not filled in correctly");
        }
        return result;
    }


    /**
     * Creates a new division from given fields.
     * Give alerts if something fails.
     * Returns true to if the division is created
     *
     * @param textFieldDivisionCategory TextField:      text field for division category.
     * @return result boolean         true if the division was created. False otherwise
     */
    public static boolean createDivision(TextField textFieldDivisionCategory) {
        boolean result = true;

        try {
            DivisionDAO divisionDAO = new DivisionDAO();
            String divisionCategory = textFieldDivisionCategory.getText();
            Division division = new Division(divisionCategory, currentCup.getCupId());
            divisionDAO.addDivision(division);
            currentCup.setDivisions(divisionDAO.getDivisionsByCup(currentCup.getCupId()));
            setCurrentDivision(division);
        } catch (IllegalArgumentException e) {
            result = false;
            giveError(e.getMessage());
        } catch (NullPointerException e) {
            result = false;
            giveError("The selected cup could not be found.");
        }

        return result;
    }

    /**
     * Creates a new match with the given fields.
     * If something fails it will give an
     * error with the ViewUtils class.
     * Returns true if the match is created.
     *
     * @param teamOneComBox  ComboBox:         A combo box with teams to set the first team
     * @param teamTwoComBox  ComboBox:         A combo box with the same teams as teamOneComBox to set the second team
     * @param startTimeField TextField:        A date field to set the start date of the match
     * @param startDateField DatePicker:       A time field to set the start time of the match
     * @param fieldField     TextField:        A text field to set the field of the match
     * @param durationField  Spinner           A spinner of Integers parsed to long to set the duration of the match
     * @param isKnockout     the is knockout
     * @return result boolean           true if the match was successfully created
     */
    public static boolean createMatch(ComboBox<Team> teamOneComBox, ComboBox<Team> teamTwoComBox, TextField startTimeField,
                                      DatePicker startDateField, TextField fieldField, Spinner<Integer> durationField,
                                      RadioButton isKnockout){
        boolean result = true;

        Team teamOne;
        Team teamTwo;
        LocalDateTime startTime;
        long duration;
        String field;
        if (teamOneComBox.getValue().equals(teamTwoComBox.getValue())) giveError("Teams cannot be the same");
        try {
            MatchDAO matchDAO = new MatchDAO();
            teamOne = teamOneComBox.getValue();
            teamTwo = teamTwoComBox.getValue();
            startTime = LocalDateTime.of(startDateField.getValue(), LocalTime.parse(startTimeField.getText()));
            duration = durationField.getValue();
            field = fieldField.getText();
            Match newMatch = new Match();
            newMatch.setTeamOne(teamOne);
            newMatch.setTeamTwo(teamTwo);
            newMatch.setStartTime(startTime);
            newMatch.setDuration(duration);
            newMatch.setField(field);
            newMatch.setWalkover(false);
            newMatch.setFinished(false);
            newMatch.setKnockout(isKnockout.selectedProperty().getValue());
            newMatch.setDivisionId(currentDivision.getDivisionId());
            matchDAO.addMatch(newMatch);
            currentDivision.setMatches(matchDAO.getMatchesByDivision(currentDivision.getDivisionId()));
        } catch (IllegalArgumentException e) {
            result = false;
            giveError(e.getMessage());
        } catch (Exception e) {
            result = false;
            giveError("Some field was not filled in correctly");
        }
        return result;
    }

    /**
     * Generate random matches.
     *
     * @param startDateField DatePicker:    the start date of the match.
     * @param startTimeField TextField:     the start time of the match.
     * @param durationField  SpinnerField:  the duration of the match.
     * @param fieldField     TextField:     the field the match is to be played on.
     * @return result boolean: true or false if the matches were generated.
     */
    public static boolean generateMatches(DatePicker startDateField, TextField startTimeField,
                                          Spinner<Integer> durationField, TextField fieldField) {
        boolean result = true;

        LocalDateTime startTime;
        long duration;

        try {
            startTime = LocalDateTime.of(startDateField.getValue(),LocalTime.parse(startTimeField.getText()));
            duration = durationField.getValue();

            int expectedAmountOfFields = getCurrentDivision().getProjectedGeneratedMatches();
            String[] fields = fieldField.getText().split(",");

            if (fields.length != expectedAmountOfFields) {
                giveError("Expected " + expectedAmountOfFields + " fields, but received " + fields.length);
                return false;
            }

            getCurrentDivision().genRandomMatches(startTime, (int) duration, fields, getCurrentDivision().getDivisionId());

        } catch (IllegalArgumentException e) {
            result = false;
            giveError(e.getMessage());
        } catch (Exception e) {
            result = false;
            giveError("Some field was not filled in correctly");
        }

        return result;
    }


    /**
     * Create team boolean.
     *
     * @param teamNameField TextField:      a text field to set the team name
     * @return result boolean         true if the team was successfully created
     */
    public static boolean createTeam(TextField teamNameField) {
        boolean result = true;

        try {
            TeamDAO teamDAO = new TeamDAO();
            String teamName = teamNameField.getText();
            Team team = new Team();
            team.setName(teamName);
            team.setCompeting(true);
            try {
                teamDAO.addTeam(team, currentDivision.getDivisionId());
                teamDAO.linkTeamToDivision(teamDAO.getTeamByName(teamName).getTeamId(), currentDivision.getDivisionId());
            } catch (IllegalArgumentException e) {
                result = false;
                giveError("Team already exists");
            }
            currentDivision.setCompetingTeams(teamDAO.getTeamsByDivision(currentDivision.getDivisionId()));
            setCurrentTeam(team);
        } catch (IllegalArgumentException e) {
            result = false;
            giveError("Team name cannot be blank.");
        }

        return result;
    }

    /**
     * Updates the score for a team in a match.
     *
     * @param team     int         team to increase score.
     * @param newScore int     new score for the team.
     */
    public static void updateMatchScore(int team, int newScore) {
        MatchDAO matchDAO = new MatchDAO();
        matchDAO.updateMatchScore(newScore, team, currentMatch.getMatchId());
        currentMatch = matchDAO.getMatchById(currentMatch.getMatchId());
        currentDivision.setMatches(matchDAO.getMatchesByDivision(currentDivision.getDivisionId()));
    }

    /**
     * Open browser.
     * Is necessary because some variants of Unix doesn't use a desktop manager and would
     * then just show a blank scene on Desktop.getDesktop().
     *
     * @param urlString String:         the url string
     * @throws IOException        the io exception
     * @throws URISyntaxException the uri syntax exception
     */
    public static void openBrowser(String urlString) throws IOException, URISyntaxException {
        if (System.getProperty("os.name").equals("Linux")) {
            /* Check if xdg-open is available. Most Linux installations based on XORG should have xdg-open
               If the installations uses Wayland or something else then we are out of luck I guess.
             */
            if (Runtime.getRuntime().exec(new String[]{"which", "xdg-open"}).getInputStream().read() != -1) {
                Runtime.getRuntime().exec(new String[]{"xdg-open", urlString});
            }
        } else {
            Desktop.getDesktop().browse(new URI(urlString));
        }
    }
}
