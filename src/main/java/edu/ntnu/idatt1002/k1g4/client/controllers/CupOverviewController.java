package edu.ntnu.idatt1002.k1g4.client.controllers;

import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.*;

import edu.ntnu.idatt1002.k1g4.Cup;
import edu.ntnu.idatt1002.k1g4.Division;
import edu.ntnu.idatt1002.k1g4.Match;
import edu.ntnu.idatt1002.k1g4.Team;
import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import static edu.ntnu.idatt1002.k1g4.client.Model.*;


/**
 * Cup Controller
 * handles the page of viewing everything in a cup.
 * Coupled up with the cup-overview.fxml file.
 *
 * @author Carl Gützkow, Nicolai H. Brand, Callum Gran
 * @version 0.6
 */
public class CupOverviewController implements Initializable {

    @FXML
    private Label cupName;

    @FXML
    private TextField cupNameField;

    @FXML
    private Label cupLocation;

    @FXML
    private Button cancelUpdateNameButton;

    @FXML
    private Button updateNameButton;

    @FXML
    private Button editCupNameButton;

    @FXML
    private ListView<Division> divisionListView = new ListView<>();

    @FXML
    private ListView<Match> matchListView = new ListView<>();

    @FXML
    private ListView<Team> teamListView = new ListView<>();

    @FXML
    private Button newButton;

    @FXML
    private Button deleteButton;

    /**
     * Add division. Changes page to the page for
     * creating a division.
     */
    @FXML
    void addDivision() {
        changeScene("create-division");
    }

    /**
     * Changes page to the home page
     */
    @FXML
    void backToCups() {
        changeScene("home");
    }

    /**
     * Called when clicking on the edit name button.
     * Reveals a text field and two buttons.
     * One for canceling the edit and one for updating the name.
     */
    @FXML
    void editCupName() {
        cupNameField.setPrefWidth(250);
        cupName.setText("");
        editCupNameButton.setScaleX(0);
        editCupNameButton.setPrefWidth(0);
        updateNameButton.setScaleX(1);
        cancelUpdateNameButton.setScaleX(1);
    }

    /**
     * Called when clicking on the update name button
     * (a checkbox). Updates the name and hides the buttons.
     */
    @FXML
    void updateName() {
        getCurrentCup().setName(cupNameField.getText());
        hideButtonsAndFieldForName();
    }

    /**
     * Called when clicking on the cancel name button
     * (a cross). Then it hides the buttons.
     */
    @FXML
    void cancelUpdateName() {
        hideButtonsAndFieldForName();
    }

    /**
     * A helper method to hide the cancel and update
     * button and then show the name.
     */
    private void hideButtonsAndFieldForName() {
        cupNameField.setPrefWidth(0);
        cupNameField.setText(getCurrentCup().getName());
        cupName.setText(getCurrentCup().getName());
        editCupNameButton.setScaleX(1);
        editCupNameButton.setPrefWidth(36);
        updateNameButton.setScaleX(0);
        cancelUpdateNameButton.setScaleX(0);
    }

    /**
     * Go to division.
     * In the initialize method we have functionality
     * to select a division by clicking on it.
     * When the button goToDivision is pressed,
     * we go into the selected division.
     */
    @FXML
    void goToDivision() {
        MatchDAO matchDAO;
        if (getCurrentDivision() != null){
            matchDAO = new MatchDAO();
            getCurrentDivision().setMatches(matchDAO.getMatchesByDivision(getCurrentDivision().getDivisionId()));
            changeScene("division-overview");
        } else {
            giveError("There is no division selected. Click on a division to select it.");
        }
    }

    /**
     * Delete division.
     * In the initialize method we have functionality
     * to select a division by clicking on it.
     * When the button deleteDivision is pressed,
     * we prompt the user if they want to delete the selected division.
     */
    @FXML
    void deleteDivision() {
        if (getCurrentDivision() == null) {
            giveError("There is no division selected. Click on a division to select it.");
        }  else {
            boolean res = getConfirmation("Are you sure you want to delete the division " + getCurrentDivision());

            if (res) {
                deleteSelectedDivision();
                /* refresh scene as the list view needs to be updated */
                changeScene("cup-overview");
            }
        }
    }

    /**
     * Helper method that initializes javafx fields
     *
     * @param currentCup    Cup:           the current cup to display
     */
    private void initScene(Cup currentCup) {
        cupNameField.setPrefWidth(0);
        cupNameField.setText(currentCup.getName());
        cupName.setText(currentCup.getName());
        cupLocation.setText(currentCup.getLocation());
        editCupNameButton.setScaleX(1);
        updateNameButton.setScaleX(0);
        cancelUpdateNameButton.setScaleX(0);
    }

    /**
     * Helper method that initializes javafx fields
     */
    private void initLists(Cup currentCup) {
        currentCup.getDivisions().forEach(n -> divisionListView.getItems().add(n));
        currentCup.getDivisions().forEach(n -> n.getMatches().forEach(m -> matchListView.getItems().add(m)));
        for (Division division : currentCup.getDivisions()) {
            for (Team team : division.getCompetingTeams().values()) {
                if (!teamListView.getItems().stream().anyMatch(n -> n.getTeamId() == team.getTeamId())) {
                    teamListView.getItems().add(team);
                }
            }
        }
    }

    /**
     * Run when the page is loaded in.
     * Fills in listviews for divisions,
     * matches and teams.
     * Adds a change listener to the division list.
     *
     * @param url               URL:                    represents a Uniform Resource Locator
     * @param resourceBundle    ResourceBundle:         contains locale-specific objects
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Cup currentCup = getCurrentCup();
        initScene(currentCup);
        initLists(currentCup);

        deleteButton.setStyle("-fx-background-color: #ff0000;");
        newButton.setStyle("-fx-background-color: #009804;");

        divisionListView.getSelectionModel().selectedItemProperty().addListener((a,b,selected) -> {
            setCurrentDivision(getCurrentCup().getEqualDivision(selected));
        });

        /* doubleclick on a division takes you straight into the division */
        divisionListView.setOnMouseClicked(click -> {
            if (click.getButton() == MouseButton.PRIMARY && click.getClickCount() == 2) {
                Division selectedDivision = divisionListView.getSelectionModel().getSelectedItem();
                if (selectedDivision != null) {
                    setCurrentDivision(selectedDivision);
                    changeScene("division-overview");
                }
            }
        });
    }
}