package edu.ntnu.idatt1002.k1g4.client;

import edu.ntnu.idatt1002.k1g4.dao.CreateDatabase;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import com.jthemedetecor.*;

import java.util.function.Consumer;


import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.*;

/**
 * Main GUI application client.
 * Class with the 'main' method
 *
 * @author Carl Gützkow, Nicolai H. Brand
 * @version 0.6
 */
public class App extends Application {
    private static final int DEFAULT_WIDTH = 1280;
    private static final int DEFAULT_HEIGHT = 720;
    private static Stage stage;

    /**
     * Method for creating a stage and the first scene.
     * Also sets title and logo.
     *
     * @param startStage Stage:         the stage provided by Application
     */
    @Override
    public void start(Stage startStage) {
        stage = startStage;
        stage.setTitle("Floorball cup manager");
        CreateDatabase.createNewDatabase("tournament.db");
        CreateDatabase.createNewTables("tournament.db");

        stage.setMaxWidth(DEFAULT_WIDTH);
        stage.setMinWidth(DEFAULT_WIDTH);
        stage.setMinHeight(DEFAULT_HEIGHT);

        Image icon = new Image("logo.png");
        stage.getIcons().add(icon);

        stage.setScene(new Scene(new AnchorPane(), DEFAULT_WIDTH, DEFAULT_HEIGHT));

        setTheme();

        changeScene("home");
        stage.show();
    }

    /**
     * Set the theme of the program.
     * Supported themes are light mode (default) and dark mode.
     */
    private static void setTheme() {
        final OsThemeDetector detector = OsThemeDetector.getDetector();
        Consumer<Boolean> darkThemeListener = isDark -> Platform.runLater(() -> {
            if (isDark)
                setCss("dark.css");
            else
                setCss("base.css");

            /* Linux users get dark mode by default as we know from empirical evidence that Linux users tend to
             * have oversensitive eyes due to a lifelong absence of direct sunlight.*/
            if (System.getProperty("os.name").equals("Linux"))
                setCss("dark.css");

            changeScene(getCurrentScene());
        });

        darkThemeListener.accept(detector.isDark());
        detector.registerListener(darkThemeListener);
    }

    /**
     * main method of the program.
     * Called when javafx:run is run
     *
     * @param args String[]           String, arguments from command
     */
    public static void main(String[] args) {
        launch();
    }

    /**
     * Returns the width that a window should have.
     * This should be used for all scenes.
     *
     * @return DEFAULT_WIDTH width of the window
     */
    public static int getDefaultWidth() {
        return DEFAULT_WIDTH;
    }

    /**
     * Returns the height that a window should have.
     * This should be used for all scenes.
     *
     * @return DEFAULT_HEIGHT height of the window
     */
    public static int getDefaultHeight() {
        return DEFAULT_HEIGHT;
    }

    /**
     * Returns the startStage to use other places
     *
     * @return stage - the stage provided by javaFX
     */
    public static Stage getStage() {
        return stage;
    }
}
