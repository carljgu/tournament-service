package edu.ntnu.idatt1002.k1g4.client.controllers;

import edu.ntnu.idatt1002.k1g4.Match;
import edu.ntnu.idatt1002.k1g4.Team;
import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import edu.ntnu.idatt1002.k1g4.dao.TeamDAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static edu.ntnu.idatt1002.k1g4.client.Model.*;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.changeScene;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.getConfirmation;

/**
 * Controller for the team overview
 * Coupled with the team-overview.fxml file
 *
 * @author Carl Gützkow
 * @version 0.1
 */
public class TeamOverviewController implements Initializable {

    @FXML
    private Label teamName;

    @FXML
    private Label division;

    @FXML
    private Label matchesFinished;

    @FXML
    private Label matchesNotFinished;

    @FXML
    private Label matchesLost;

    @FXML
    private Label matchesWon;

    @FXML
    private Label matchesDraw;

    /**
     * changes the scene back to the division overview
     */
    @FXML
    void backToDivisionOverview() {
        changeScene("division-overview");
    }

    /**
     * Deletes the team if user confirms
     * Then it changes to the division overview page
     */
    @FXML
    void deleteTeam() {
        boolean rc = getConfirmation("Are you sure you want to delete the team " + getCurrentTeam().getName());
        if (rc) {
            TeamDAO teamDAO = new TeamDAO();
            MatchDAO matchDAO = new MatchDAO();
            teamDAO.deleteTeam(getCurrentTeam().getTeamId(), getCurrentDivision().getDivisionId());
            getCurrentDivision().setCompetingTeams(teamDAO.getTeamsByDivision(getCurrentDivision().getDivisionId()));
            getCurrentDivision().setMatches(matchDAO.getMatchesByDivision(getCurrentDivision().getDivisionId()));
            changeScene("division-overview");
        }
    }

    /**
     * Run when the page is loaded in.
     * Fills in numbers for what
     * matches the team are a part of
     *
     * @param url               URL:                        represents a Uniform Resource Locator
     * @param resourceBundle    ResourceBundle:             contains locale-specific objects
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Team team = getCurrentTeam();
        teamName.setText(team.getName());
        division.setText(getCurrentDivision().getDivisionCategory());

        ArrayList<Match> matchesWithTeam = getCurrentDivision().getMatches().stream()
                .filter(m -> (m.getTeamOne().equals(team) || m.getTeamTwo().equals(team))
                ).collect(Collectors.toCollection(ArrayList::new));
        matchesNotFinished.setText(String.valueOf(
                matchesWithTeam.stream().filter(m -> !m.isFinished()).count())
        );
        matchesFinished.setText(String.valueOf(
                matchesWithTeam.stream().filter(Match::isFinished).count())
        );
        matchesWon.setText(String.valueOf(matchesWithTeam.stream().filter(m -> m.isFinished() && (
                m.getScore(team) > m.getScores()[0] || m.getScore(team) > m.getScores()[1]
        )).count()));
        matchesLost.setText(String.valueOf(matchesWithTeam.stream().filter(m -> m.isFinished() && (
                m.getScore(team) < m.getScores()[0] || m.getScore(team) < m.getScores()[1]
        )).count()));
        matchesDraw.setText(String.valueOf(matchesWithTeam.stream().filter(m -> m.isFinished() && (
                m.getScore(team) == m.getScores()[0] && m.getScore(team) == m.getScores()[1]
        )).count()));

    }
}