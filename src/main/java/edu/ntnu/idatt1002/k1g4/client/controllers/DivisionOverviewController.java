package edu.ntnu.idatt1002.k1g4.client.controllers;

import edu.ntnu.idatt1002.k1g4.*;
import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import edu.ntnu.idatt1002.k1g4.dao.TeamDAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;

import java.net.URL;
import java.util.ResourceBundle;

import static edu.ntnu.idatt1002.k1g4.client.Model.*;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.*;


/**
 * Controller for viewing details about
 * a division. The current division is
 * stored in the Model class.
 *
 * @author Carl Gützkow, Nicolai H. Brand, Runar Indahl, Callum Gran
 * @version 0.8
 */
public class DivisionOverviewController implements Initializable {

    @FXML
    private Label divisionTypeLabel;

    @FXML
    private ListView<Match> matchList;

    @FXML
    private ListView<Team> teamList;

    @FXML
    private ListView<Match> resultsList;

    @FXML
    private Button deleteMatchButton;

    @FXML
    private Button deleteTeamButton;

    @FXML
    private Button newMatchButton;

    @FXML
    private Button newTeamButton;

    @FXML
    private Button generateButton;

    /**
     * Changes page to the match overview
     * if there is no current match it
     * gives an error.
     */
    @FXML
    void goToMatch() {
        if (getCurrentMatch() != null) {
            changeScene("match-overview");
        }
        else
            giveError("There is no match selected. Click on a match to select it.");
    }

    /**
     * Changes page to page for creating team.
     */
    @FXML
    void addTeam() {
        changeScene("create-team");
    }

    /**
     * Changes page back to the cup
     * overview scene without any changes
     */
    @FXML
    void backToCupOverview() {
        changeScene("cup-overview");
    }

    /**
     * Changes page to the create match page
     * where a match can be created.
     */
    @FXML
    void createMatch() {
        changeScene("create-match");
    }

    @FXML
    void generateMatch() {
        changeScene("generate-matches");
    }

    /**
     * Deletes division if the user confirms the action
     * Sets the current division to null
     */
    @FXML
    void deleteDivision() {
        if (getConfirmation("Are you sure you want to delete this division?")) {
            getCurrentCup().removeDivision(getCurrentDivision());
            setNoCurrentDivision();
            changeScene("cup-overview");
        }
    }

    /**
     * Deletes the current match if
     * user confirms the action.
     */
    @FXML
    void deleteMatch() {
        if (getCurrentMatch() == null) {
            giveError("There is no match selected. Click on a match to select it.");
        } else {
            if (getConfirmation("Are you sure you want to delete this match?")) {
                deleteSelectedMatch();
                setNoCurrentMatch();
                /* refresh scene to update listview */
                changeScene("division-overview");
            }
        }
    }

    /**
     * Changes scene to the team overview if a team is selected
     * Otherwise, it gives an error
     */
    @FXML
    void goToTeam() {
        if (getCurrentTeam() == null) {
            giveError("There is no team selected. Click on a team to select it.");
        }  else {
            changeScene("team-overview");
        }
    }

    /**
     * Deletes a team from the selected division
     * if a team is selected and user confirms the action
     */
    @FXML
    void deleteTeam() {
        if (getCurrentTeam() == null) {
            giveError("There is no team selected. Click on a team to select it.");
        }  else {
            boolean rc = getConfirmation("Are you sure you want to delete the team " + getCurrentTeam().getName());
            if (rc) {
                TeamDAO teamDAO = new TeamDAO();
                MatchDAO matchDAO = new MatchDAO();
                teamDAO.deleteTeam(getCurrentTeam().getTeamId(), getCurrentDivision().getDivisionId());
                getCurrentDivision().setCompetingTeams(teamDAO.getTeamsByDivision(getCurrentDivision().getDivisionId()));
                getCurrentDivision().setMatches(matchDAO.getMatchesByDivision(getCurrentDivision().getDivisionId()));
                changeScene("division-overview");
            }
        }
    }


    /**
     * Updates the items in all the lists.
     * Fills in matches and teams in the lists
     */
    private void updateItemsInLists() {
        getCurrentDivision().getMatches().forEach(n -> matchList.getItems().add(n));
        getCurrentDivision().getCompetingTeams().forEach((n, m) -> teamList.getItems().add(m));
        getCurrentDivision().getMatches().stream().filter(Match::isFinished).forEach(n -> resultsList.getItems().add(n));
    }

    /**
     * When clicking on an item in a list, this item is selected as the currentItem.
     * When double clicking on an item in a list, this item is selected sa the currentItem and
     * changes to the corresponding scene of that selected item.
     */
    private void selectItemsInList() {

        /* when a match/team is selected, set it as the current match/team */
        matchList.getSelectionModel().selectedItemProperty().addListener((a, b, match) -> setCurrentMatch(match));
        teamList.getSelectionModel().selectedItemProperty().addListener((a, b, team) -> setCurrentTeam(team));

        newMatchButton.setStyle("-fx-background-color: #009804;");
        newTeamButton.setStyle("-fx-background-color: #009804;");
        generateButton.setStyle("-fx-background-color: #ff6a00;");
        deleteMatchButton.setStyle("-fx-background-color: #ff0000;");
        deleteTeamButton.setStyle("-fx-background-color: #ff0000;");

        /* if double click on match, go straight into it */
        matchList.setOnMouseClicked(click -> {
            if (click.getButton() == MouseButton.PRIMARY && click.getClickCount() == 2) {
                Match selectedMatch = matchList.getSelectionModel().getSelectedItem();
                if (selectedMatch != null) {
                    setCurrentMatch(selectedMatch);
                    changeScene("match-overview");
                }
            }
        });

        /* if double click on team, go straight into it */
        teamList.setOnMouseClicked(click -> {
            if (click.getButton() == MouseButton.PRIMARY && click.getClickCount() == 2) {
                Team selectedTeam = teamList.getSelectionModel().getSelectedItem();
                if (selectedTeam != null) {
                    setCurrentTeam(selectedTeam);
                    changeScene("team-overview");
                }
            }
        });
    }

    /**
     * Run when the page is loaded in.
     * Updates the lists and selection models
     *
     * @param url               URL:                        represents a Uniform Resource Locator
     * @param resourceBundle    ResourceBundle:             contains locale-specific objects
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setCurrentDivision(getCurrentCup().getEqualDivision(getCurrentDivision()));
        divisionTypeLabel.setText(getCurrentDivision().getDivisionCategory());

        /* update items in lists */
        updateItemsInLists();
        selectItemsInList();
    }
}
