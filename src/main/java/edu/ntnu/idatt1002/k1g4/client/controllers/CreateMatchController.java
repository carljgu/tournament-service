package edu.ntnu.idatt1002.k1g4.client.controllers;

import edu.ntnu.idatt1002.k1g4.Division;
import edu.ntnu.idatt1002.k1g4.Team;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

import static edu.ntnu.idatt1002.k1g4.client.Model.createMatch;
import static edu.ntnu.idatt1002.k1g4.client.Model.getCurrentDivision;
import static edu.ntnu.idatt1002.k1g4.client.ViewUtil.changeScene;

/**
 * Controller for creating a match
 * Coupled with the create-match.fxml file
 *
 * @author Eilert Werner Hansen
 * @version 0.1
 */
public class CreateMatchController implements Initializable {

    @FXML
    private Spinner<Integer> durationField = new Spinner<>();

    @FXML
    private TextField fieldField;

    @FXML
    private DatePicker startDateField;

    @FXML
    private TextField startTimeField;

    @FXML
    private ComboBox<Team> teamOneComBox = new ComboBox<>();

    @FXML
    private ComboBox<Team> teamTwoCombox = new ComboBox<>();

    @FXML
    private RadioButton isKnockout;

    /**
     * Cancel the creation of
     * a match and returns user
     * to the division overview.
     */
    @FXML
    void cancelCreation() {
        changeScene("division-overview");
    }

    /**
     * Creates a match with the Model class
     * returns the user to the division overview page.
     */
    @FXML
    void onCreateMatch() {
            if (createMatch(teamOneComBox, teamTwoCombox, startTimeField, startDateField, fieldField, durationField,
                    isKnockout)){
                changeScene("division-overview");
            }
    }

    /**
     * Run when the page is loaded in.
     * Fills in the combo box and spinner.
     * Fills in combo boxes and spinners
     *
     * @param url               URL:                        represents a Uniform Resource Locator
     * @param resourceBundle    ResourceBundle:             contains locale-specific objects
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Division currentDivision = getCurrentDivision();
        teamOneComBox.getItems().addAll(currentDivision.getCompetingTeams().values().stream().filter(team -> team.isCompeting() == true).toList());
        teamTwoCombox.getItems().addAll(currentDivision.getCompetingTeams().values().stream().filter(team -> team.isCompeting() == true).toList());
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100);
        valueFactory.setValue(0);
        durationField.setValueFactory(valueFactory);
        durationField.increment(24);
    }
}
