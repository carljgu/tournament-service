package edu.ntnu.idatt1002.k1g4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CupTest {

    /* Default values for test*/
    private final static String name = "Dragvoll cup";
    private final static String location = "Dragvoll";
    private ArrayList<Division> divisions;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    @BeforeEach
    public void prepareLegalTest() {
        divisions = new ArrayList<>();
        startTime = LocalDateTime.now();
        endTime = startTime.plusMinutes(5);
    }

    @Nested
    public class CupConstructorTest {

        @Test
        public void validArgumentsCreateNewInstanceOfCup() {
            try {
                Cup cup = new Cup(name, location, divisions, startTime, endTime);
                assertTrue(true);
            } catch (Exception e) {
                fail();
            }
        }

        @Test
        public void cupConstructorThrowsException() {
            /* End time is before start time */
            endTime = startTime.plusMinutes(-1);
            try {
                Cup cup = new Cup(name, location, divisions, startTime, endTime);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("End time cannot be before start time", e.getMessage());
            }
        }
    }
}
