package edu.ntnu.idatt1002.k1g4;

import edu.ntnu.idatt1002.k1g4.dao.Database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTestDB {
    private static Connection connection;
    private static Statement statement;

    public static void setUp() {
        try {
            connection = Database.instance().getConnection();
            statement = connection.createStatement();

        } catch (SQLException se) {
            System.out.println("Could not connect to test-database");
            se.printStackTrace();
        }

        String dropTeams = "DROP TABLE IF EXISTS teams";
        String dropCups = "DROP TABLE IF EXISTS cups";
        String dropDivisions = "DROP TABLE IF EXISTS divisions";
        String dropMatches = "DROP TABLE IF EXISTS matches";
        String dropDivisionsTeams = "DROP TABLE IF EXISTS divisionsteamslink";

        String teamsSql = "CREATE TABLE IF NOT EXISTS teams (\n"
                + "	teamId integer PRIMARY KEY,\n"
                + "	teamName text\n"
                + ");";


        String cupsSql = "CREATE TABLE IF NOT EXISTS cups (\n"
                + " cupId integer PRIMARY KEY,\n"
                + " cupName text,\n"
                + " location text,\n"
                + " cupStartTime text,\n"
                + " cupEndTime text\n"
                + ");";

        String divisionsSql = "CREATE TABLE IF NOT EXISTS divisions (\n"
                + " divisionId integer PRIMARY KEY,\n"
                + " cupId integer,\n"
                + " class text,\n"
                + " FOREIGN KEY (cupId) REFERENCES cup(cupId)\n"
                + ");";

        String matchesSql = "CREATE TABLE IF NOT EXISTS matches (\n"
                + " matchId integer PRIMARY KEY,\n"
                + " divisionId integer,\n"
                + " team1Id integer, \n"
                + " team2Id integer, \n"
                + " scoreTeam1 integer,\n"
                + " scoreTeam2 integer,\n"
                + " matchStartTime text, \n"
                + " matchEndTime text, \n"
                + " duration BIGINT, \n"
                + " fieldNumber text, \n"
                + " finished bit,\n"
                + " walkover bit,\n"
                + " knockout bit,\n"
                + " FOREIGN KEY (divisionId) REFERENCES divisions(divisionId)\n"
                + ");";

        String divisionsTeamsSql = "CREATE TABLE IF NOT EXISTS divisionsteamslink(\n"
                + " divisionId integer,\n"
                + " teamId integer,\n"
                + " competing bit, \n"
                + " FOREIGN KEY (divisionId) REFERENCES divisions(divisionId),\n"
                + " FOREIGN KEY (teamId) REFERENCES teams(teamId),\n"
                + " PRIMARY KEY (divisionId, teamId)\n"
                + ");";


        String insertTeam = "INSERT INTO teams (teamName) VALUES ('Jims Warriors')";
        String insertTeam2 = "INSERT INTO teams (teamName) VALUES ('Rogers Warriors')";
        String insertTeam3 = "INSERT INTO teams (teamName) VALUES ('Tommys Krigere')";
        String insertTeam4 = "INSERT INTO teams (teamName) VALUES ('Jeremies Fighters')";
        String insertMatch = "INSERT INTO matches (divisionId, team1Id, team2Id, scoreTeam1, scoreTeam2, matchStartTime, matchEndTime, duration, fieldNumber, finished, walkover, knockout) VALUES (1, 1, 2, 0, 0, '2007-12-03T10:15:30', '2007-12-03T10:15:50', 20, 'A1', 1, 1, 1)";
        String insertMatch2 = "INSERT INTO matches (divisionId, team1Id, team2Id, scoreTeam1, scoreTeam2, matchStartTime, matchEndTime, duration, fieldNumber, finished, walkover, knockout) VALUES (1, 2, 3, 0, 0, '2008-12-03T10:15:30', '2008-12-03T10:15:50', 20, 'A2', 1, 1, 1)";
        String insertMatch3 = "INSERT INTO matches (divisionId, team1Id, team2Id, scoreTeam1, scoreTeam2, matchStartTime, matchEndTime, duration, fieldNumber, finished, walkover, knockout) VALUES (1, 1, 4, 0, 0, '2009-12-03T10:15:30', '2009-12-03T10:15:50', 20, 'A3', 1, 1, 1)";
        String insertDivision = "INSERT INTO divisions (cupId, class) VALUES (1, 'Boys Under19')";
        String insertDivision2 = "INSERT INTO divisions (cupId, class) VALUES (1, 'Menn Under21')";
        String insertDivision3 = "INSERT INTO divisions (cupId, class) VALUES (1, 'Menn Senior')";
        String insertDivision4 = "INSERT INTO divisions (cupId, class) VALUES (2, 'Boys Under21')";
        String insertCup = "INSERT INTO cups (cupName, location, cupStartTime, cupEndTime) VALUES ('Dragvoll Cup', 'Dragvoll', '2007-12-03T10:15:30', '2007-12-05T10:15:30')";
        String insertCup2 = "INSERT INTO cups (cupName, location, cupStartTime, cupEndTime) VALUES ('Inne Cup', 'Oslo', '2009-12-03T10:15:30', '2009-12-05T10:15:30')";
        String insertCup3 = "INSERT INTO cups (cupName, location, cupStartTime, cupEndTime) VALUES ('Bandy Cup', 'Trondheim', '2009-12-03T10:15:30', '2009-12-05T10:15:30')";
        String linkTeamsDivisions = "INSERT INTO divisionsteamslink (divisionId, teamId, competing) VALUES (1, 1, 1)";
        String linkTeamsDivisions2 = "INSERT INTO divisionsteamslink (divisionId, teamId, competing) VALUES (1, 2, 1)";
        String linkTeamsDivisions3 = "INSERT INTO divisionsteamslink (divisionId, teamId, competing) VALUES (1, 3, 1)";
        String linkTeamsDivisions4 = "INSERT INTO divisionsteamslink (divisionId, teamId, competing) VALUES (1, 4, 1)";


        try {
            statement.executeUpdate(dropTeams);
            statement.executeUpdate(dropCups);
            statement.executeUpdate(dropDivisions);
            statement.executeUpdate(dropMatches);
            statement.executeUpdate(dropDivisionsTeams);

            statement.executeUpdate(teamsSql);
            statement.executeUpdate(cupsSql);
            statement.executeUpdate(divisionsSql);
            statement.executeUpdate(matchesSql);
            statement.executeUpdate(divisionsTeamsSql);

            statement.executeUpdate(insertTeam);
            statement.executeUpdate(insertTeam2);
            statement.executeUpdate(insertTeam3);
            statement.executeUpdate(insertTeam4);
            statement.executeUpdate(insertMatch);
            statement.executeUpdate(insertMatch2);
            statement.executeUpdate(insertMatch3);
            statement.executeUpdate(insertCup);
            statement.executeUpdate(insertCup2);
            statement.executeUpdate(insertCup3);
            statement.executeUpdate(insertDivision);
            statement.executeUpdate(insertDivision2);
            statement.executeUpdate(insertDivision3);
            statement.executeUpdate(insertDivision4);
            statement.executeUpdate(linkTeamsDivisions);
            statement.executeUpdate(linkTeamsDivisions2);
            statement.executeUpdate(linkTeamsDivisions3);
            statement.executeUpdate(linkTeamsDivisions4);

        } catch (SQLException se) {
            System.out.println("Error: insert statements");
            se.printStackTrace();

        } finally {
            try {
                statement.close();

            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
