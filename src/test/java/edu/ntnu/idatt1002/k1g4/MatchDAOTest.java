package edu.ntnu.idatt1002.k1g4;

import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class MatchDAOTest {
    private MatchDAO matchDAO;

    @BeforeEach
    public void setUp() {
        matchDAO = new MatchDAO();
        CreateTestDB.setUp();
    }

    @Test
    public void testGetMatchesByCup() {
        ArrayList<Match> matches = matchDAO.getMatchesByCup(1);
        List<String> actualMatches = new ArrayList<>();

        for(Match match : matches) {
            actualMatches.add(match.getField());
        }

        List<String> expectedFieldList = new ArrayList<>();
        expectedFieldList.add("A1");
        expectedFieldList.add("A2");
        expectedFieldList.add("A3");
        assertTrue(actualMatches.size() == expectedFieldList.size() && actualMatches.containsAll(expectedFieldList));
    }

    @Test
    public void testGetMatchesByDivision() {
        ArrayList<Match> matches = matchDAO.getMatchesByDivision(1);
        List<String> actualMatches = new ArrayList<>();

        for(Match match : matches) {
            actualMatches.add(match.getField());
        }

        List<String> expectedFieldList = new ArrayList<>();
        expectedFieldList.add("A1");
        expectedFieldList.add("A2");
        expectedFieldList.add("A3");
        assertTrue(actualMatches.size() == expectedFieldList.size() && actualMatches.containsAll(expectedFieldList));
    }

    @Test
    public void testMatchMakesOneTeamNotCompetingAfterLoss() {
        Match match = matchDAO.getMatchById(1);
        match.setScore(new int[]{1, 3});
        match.setFinished();
        matchDAO.updateMatch(match);
        match = matchDAO.getMatchById(1);
        Team losingTeam = match.getTeamOne();
        Team winningTeam = match.getTeamTwo();
        assertTrue(winningTeam.isCompeting());
        assertFalse(losingTeam.isCompeting());
    }


    @Test
    public void testAddMatch() {
        Team teamOne = new Team( "Team One", true);
        Team teamTwo = new Team( "Team Two", true);
        teamOne.setTeamId(1);
        teamTwo.setTeamId(2);
        LocalDateTime startTime = LocalDateTime.now();
        Match match = new Match(teamOne, teamTwo, startTime, 20, "1");
        match.setDivisionId(1);
        matchDAO.addMatch(match);
        ArrayList matches = matchDAO.getMatchesByDivision(1);
        assertTrue(matches.size() == 4);
    }

    @Test
    public void testGetMatchById() {
        String field = matchDAO.getMatchById(1).getField();
        assertEquals(field, "A1");
    }
}