package edu.ntnu.idatt1002.k1g4;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class TeamTest {

    @Nested
    public class TeamConstructorThrowsException {
        @Test
        public void teamCannotHaveBlankName() {
            try {
                Team team = new Team(" ", true);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Team needs a name", e.getMessage());
            }
        }
    }
}
