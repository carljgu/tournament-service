package edu.ntnu.idatt1002.k1g4;

import edu.ntnu.idatt1002.k1g4.dao.CupDAO;

import edu.ntnu.idatt1002.k1g4.dao.DivisionDAO;
import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CupDAOTest {
    private CupDAO cupDAO;

    @BeforeEach
    public void setUp() {
        cupDAO = new CupDAO();
        CreateTestDB.setUp();
    }

    @Test
    public void testGetCups() {
        ArrayList<Cup> cups = cupDAO.getCups();
        List<String> actualCups = new ArrayList<>();

        for(Cup cup : cups) {
            actualCups.add(cup.getName());
        }

        List<String> expectedNameList = new ArrayList<>();
        expectedNameList.add("Dragvoll Cup");
        expectedNameList.add("Inne Cup");
        expectedNameList.add("Bandy Cup");
        assertTrue(actualCups.size() == expectedNameList.size() && actualCups.containsAll(expectedNameList));
    }

    @Test
    public void testAddCup() {
        ArrayList<Cup> cupsBefore = cupDAO.getCups();
        Cup cup = new Cup("Ball Cup", "Dragvoll", LocalDateTime.parse("2007-12-03T10:15:30"), LocalDateTime.parse("2007-12-04T10:15:30"));
        cupDAO.addCup(cup);
        ArrayList<Cup> cups = cupDAO.getCups();
        assertTrue(cups.size() == 4 && cupsBefore.size() == 3);
    }

    @Test
    public void testDeleteCup() {
        MatchDAO matchDAO = new MatchDAO();
        DivisionDAO divisionDAO = new DivisionDAO();
        ArrayList<Cup> cupsBefore = cupDAO.getCups();
        ArrayList<Match> matchesBefore = matchDAO.getMatchesByCup(1);
        ArrayList<Division> divisionsBefore = divisionDAO.getDivisionsByCup(1);
        cupDAO.deleteCup(cupsBefore.get(0).getCupId());
        assertTrue(cupDAO.getCups().size() == cupsBefore.size() - 1);
        assertEquals(0, matchDAO.getMatchesByCup(1).size());
        assertEquals(0, divisionDAO.getDivisionsByCup(1).size());
        assertNotEquals(divisionsBefore.size(), divisionDAO.getDivisionsByCup(1).size());
        assertNotEquals(matchesBefore.size(), matchDAO.getMatchesByCup(1).size());
    }
}