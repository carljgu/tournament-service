package edu.ntnu.idatt1002.k1g4;

import edu.ntnu.idatt1002.k1g4.dao.DivisionDAO;
import edu.ntnu.idatt1002.k1g4.dao.MatchDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DivisionDAOTest {
    private DivisionDAO divisionDAO;

    @BeforeEach
    public void setUp() {
        divisionDAO = new DivisionDAO();
        CreateTestDB.setUp();
    }

    @Test
    public void testGetDivisionsByCup() {
        ArrayList<Division> divisions = divisionDAO.getDivisionsByCup(1);
        List<String> actualDivisions = new ArrayList<>();

        for(Division division : divisions) {
            actualDivisions.add(division.getDivisionCategory());
        }

        List<String> expectedDivisionList = new ArrayList<>();
        expectedDivisionList.add("Boys Under19");
        expectedDivisionList.add("Menn Under21");
        expectedDivisionList.add("Menn Senior");
        assertTrue(actualDivisions.size() == expectedDivisionList.size() && actualDivisions.containsAll(expectedDivisionList));
    }

    @Test
    public void testAddDivision() {
        ArrayList<Division> divisionsBefore = divisionDAO.getDivisionsByCup(1);
        Division division = new Division("Women senior", 1);
        divisionDAO.addDivision(division);
        ArrayList<Division> divisions = divisionDAO.getDivisionsByCup(1);
        assertTrue(divisions.size() == 4 && divisionsBefore.size() == 3);
    }

    @Test
    public void testDeleteDivision() {
        MatchDAO matchDAO = new MatchDAO();
        ArrayList<Division> divisionsBefore = divisionDAO.getDivisionsByCup(1);
        ArrayList<Match> matchesBefore = matchDAO.getMatchesByDivision(1);
        divisionDAO.deleteDivision(divisionsBefore.get(0).getDivisionId());
        assertTrue(divisionDAO.getDivisionsByCup(1).size() == divisionsBefore.size() - 1);
        assertNotEquals(matchesBefore.size(), matchDAO.getMatchesByDivision(1).size());
    }
}