package edu.ntnu.idatt1002.k1g4;

import edu.ntnu.idatt1002.k1g4.dao.TeamDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class TeamDAOTest {
    private TeamDAO teamDAO;

    @BeforeEach
    public void setUp() {
        teamDAO = new TeamDAO();
        CreateTestDB.setUp();
    }

    @Test
    public void testGetTeams() {
        List<Team> teams = teamDAO.getTeams();
        List<String> actualTeams = new ArrayList<>();

        for(Team team : teams) {
            actualTeams.add(team.getName());
        }

        List<String> expectedNameList = new ArrayList<>();
        expectedNameList.add("Jims Warriors");
        expectedNameList.add("Rogers Warriors");
        expectedNameList.add("Tommys Krigere");
        expectedNameList.add("Jeremies Fighters");
        assertTrue(actualTeams.size() == expectedNameList.size() && actualTeams.containsAll(expectedNameList));
    }

    @Test
    public void testGetTeamByName() {
        Team team = teamDAO.getTeamByName("Jims Warriors");
        assertEquals("Jims Warriors", team.getName());
    }

    @Test
    public void testGetTeamsByDivision() {
        HashMap<Integer, Team> teams = teamDAO.getTeamsByDivision(1);
        List<String> actualTeams = new ArrayList<>();

        teams.forEach((key, value) -> {
            actualTeams.add(value.getName());
        });

        List<String> expectedNameList = new ArrayList<>();
        expectedNameList.add("Jims Warriors");
        expectedNameList.add("Rogers Warriors");
        expectedNameList.add("Tommys Krigere");
        expectedNameList.add("Jeremies Fighters");
        assertTrue(actualTeams.size() == expectedNameList.size() && actualTeams.containsAll(expectedNameList));
    }

    @Test
    public void testAddTeam() {
        Team team = new Team("Callums Javaer", true);
        try {
            teamDAO.addTeam(team, 1);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        List<Team> teams = teamDAO.getTeams();
        assertEquals(5, teams.size());
    }
}