package edu.ntnu.idatt1002.k1g4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class DivisionTest {

    Team testTeam1;
    Team testTeam2;
    Match testMatch;
    @BeforeEach
    public void createsInitialTeamsAndAMatchToUseForFurtherTesting() {
        testTeam1 = new Team("TestTeam1", true);
        testTeam2 = new Team("TestTeam2", true);
        testTeam1.setTeamId(1);
        testTeam2.setTeamId(2);

        testMatch = new Match(testTeam1, testTeam2,
                LocalDateTime.of(2022,4,29,19, 0),
                25L,"3", false);
    }

    @Nested
    public class AddAndRemoveTeamTest {

        @Test
        public void addNewTeamToADivision() {
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addTeam(testTeam1));
            assertTrue(testDivision.addTeam(testTeam2));
        }

        @Test
        public void removeTeamThatIsNotInDivision() {
            Division testDivision = new Division("Division 1", 1);
            assertFalse(testDivision.removeTeam(testTeam1));
        }

        @Test
        public void removeTeamThatIsInDivision() {
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addTeam(testTeam1));
            assertTrue(testDivision.removeTeam(testTeam1));
        }

        @Test
        public void isCompetingStatusInTeamIsSetToTrueAfterAddingToDivision() {
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addTeam(testTeam1));
            assertTrue(testTeam1.isCompeting());
        }

        @Test
        public void isCompetingStatusInTeamIsSetToFalseAfterRemovingFromDivision() {
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addTeam(testTeam1));
            assertTrue(testDivision.removeTeam(testTeam1));
            assertFalse(testTeam1.isCompeting());
        }
    }

    @Nested
    public class AddAndRemoveMatches {

        @Test
        public void addNewMatchToADivision() {
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addMatch(testMatch));
        }

        @Test
        public void removeMatchFromDivision() {
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addMatch(testMatch));
            assertTrue(testDivision.removeMatch(testMatch));
        }
    }

    @Nested
    public class gettersTest {

        @Test
        public void getCompetingTeamsMakesADeepCopy() {
            /*
            The method getCompetingTeams() makes a deep copy of the HashMap<String, Team> that contains teams in
            the division. This method first creates two HashMaps that are identical. When this test
            removes one team from one of the HashMap, it will not be removed from the other HashMap,
            unless it is a shallow copy.
             */
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addTeam(testTeam1));
            assertTrue(testDivision.addTeam(testTeam2));
            HashMap<Integer, Team> alreadyCompetingTeams = testDivision.getCompetingTeams();

            HashMap<Integer, Team> newCompetingTeams = new HashMap<>();
            newCompetingTeams.put(testTeam1.getTeamId(),testTeam1);
            newCompetingTeams.put(testTeam2.getTeamId(),testTeam2);

            alreadyCompetingTeams.remove(testTeam1.getTeamId(),testTeam1);

            assertNotEquals(alreadyCompetingTeams, newCompetingTeams);
        }

        @Test
        public void getMatchesMakesACopy() {
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addMatch(testMatch));

            ArrayList<Match> matchList = new ArrayList<>();
            matchList.add(testMatch);

            assertEquals(matchList,testDivision.getMatches());
        }

        @Test
        public void getMatchesMakesADeepCopy() {
            Division testDivision = new Division("Division 1", 1);
            assertTrue(testDivision.addMatch(testMatch));

            ArrayList<Match> matchList = new ArrayList<>();
            matchList.add(testMatch);

            testDivision.removeMatch(testMatch);

            assertNotEquals(matchList,testDivision.getMatches());
        }
    }
}