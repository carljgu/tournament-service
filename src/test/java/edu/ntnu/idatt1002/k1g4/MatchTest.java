package edu.ntnu.idatt1002.k1g4;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class MatchTest {

    Team teamOne;
    Team teamTwo;
    LocalDateTime startTime = LocalDateTime.now();
    long duration = 25L;

    @BeforeEach
    public void createSampleTeams() {
        teamOne = new Team( "Team One", true);
        teamTwo = new Team( "Team Two", true);
        teamOne.setTeamId(1);
        teamTwo.setTeamId(2);
    }

    @Nested
    public class MatchConstructorThrowsException {

        @Test
        public void fieldCannotBeEmpty() {
            try {
                Match match = new Match(teamOne, teamTwo, startTime, duration, "", false);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Field is invalid", e.getMessage());
            }
        }

        @Test
        public void teamCannotBeNull() {
            teamOne = null;
            teamTwo = null;
            try {
                Match match = new Match(teamOne, teamTwo, startTime, duration, "1");
                fail();
            } catch (NullPointerException e) {
                assertEquals("Team is not defined", e.getMessage());
            }
        }

        @Test
        public void teamsCannotBeTheSame() {

            try {
                Match match = new Match(teamOne, teamOne, startTime, duration, "1");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Teams cannot be the same", e.getMessage());
            }
        }

        @Test
        public void EndTimeCannotBeforeStartTime() {

            try {
                Match match = new Match(teamOne, teamTwo, startTime, -2L, "1");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Duration can not be negative", e.getMessage());
            }
        }

        @Test
        public void TeamsMustBeCompeting() {

            teamOne.setCompeting(false);
            try {
                Match match = new Match(teamOne, teamTwo, startTime, duration, "1");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("teams must be competing", e.getMessage());
            }
        }

    }

    @Nested
    public class ScoreTests {

        Match match;

        @BeforeEach
        public void initiateMatch() {
            match = new Match(teamOne, teamTwo, startTime, duration, "1");
        }

        @Test
        public void getScore() {
            assertEquals(0, match.getScore(teamOne));
            match.incrementScore(teamOne);
            assertEquals(1, match.getScore(teamOne));
            match.incrementScore(teamOne);
            assertEquals(2, match.getScore(teamOne));

            assertEquals(0, match.getScore(teamTwo));
            match.incrementScore(teamTwo);
            assertEquals(1, match.getScore(teamTwo));
            match.incrementScore(teamTwo);
            assertEquals(2, match.getScore(teamTwo));
            match.incrementScore(teamTwo);
        }
    }

}
